package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "type_packs")
public class TypePacksEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "libelle")
	private String libelle;
	@Column(name = "quantite_mads")
	private int quantiteMads;
	@Column(name = "prix")
	private float prix;
	
	@OneToMany (mappedBy = "typePacksEntity",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<TransactionEntity> transaction;
	
	public TypePacksEntity(Integer id, String libelle, int quantiteMads, float prix) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.quantiteMads = quantiteMads;
		this.prix = prix;
	}
	public TypePacksEntity() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public int getQuantiteMads() {
		return quantiteMads;
	}
	public void setQuantiteMads(int quantiteMads) {
		this.quantiteMads = quantiteMads;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + Float.floatToIntBits(prix);
		result = prime * result + quantiteMads;
		result = prime * result + ((transaction == null) ? 0 : transaction.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypePacksEntity other = (TypePacksEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (Float.floatToIntBits(prix) != Float.floatToIntBits(other.prix))
			return false;
		if (quantiteMads != other.quantiteMads)
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TypePacksEntity [id=" + id + ", libelle=" + libelle + ", quantiteMads=" + quantiteMads + ", prix="
				+ prix + ", transaction=" + transaction + "]";
	}

}
