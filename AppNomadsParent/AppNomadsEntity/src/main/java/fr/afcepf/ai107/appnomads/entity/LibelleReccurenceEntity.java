package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table (name = "libelle_reccurence")
public class LibelleReccurenceEntity implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	int id;
	@Column (name = "libelle")
	String libelle;
	@OneToMany (mappedBy = "libelleReccurence",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<ReccurenceEntity> reccurences;

	/**
	 * @param idLibelleReccurence
	 * @param libelle
	 * @param reccurences
	 */
	public LibelleReccurenceEntity(int idLibelleReccurence, String libelle, List<ReccurenceEntity> reccurences)
	{
		super();
		this.id = idLibelleReccurence;
		this.libelle = libelle;
		this.reccurences = reccurences;
	}
	

	/**
	 * 
	 */
	public LibelleReccurenceEntity()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	public int getIdLibelleReccurence() {
		return id;
	}

	public void setIdLibelleReccurence(int idLibelleReccurence) {
		this.id = idLibelleReccurence;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<ReccurenceEntity> getReccurences() {
		return reccurences;
	}

	public void setReccurences(List<ReccurenceEntity> reccurences) {
		this.reccurences = reccurences;
	}
	
	
}
