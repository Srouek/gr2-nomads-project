package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "reccurence")
public class ReccurenceEntity implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", nullable = false)
	private Integer id;
	@Column (name = "duree_reccurence")
	int dureeReccurence; // Je ne suis pas d'aacord avec le int car les reccurence se calcule sur le temps, on devrait donc utiliser un type Date ou OffSetDateTime
	@Column (name = "ecart_reccurence")
	int ecartReccurence; // De même sur cette attribut
	
	@OneToMany (mappedBy = "reccurence",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<PrestationEntity> prestations;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	LibelleReccurenceEntity libelleReccurence;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	LibelleDureeReccurenceEntity libelleDureeReccurence;
	/**
	 * @param idReccurence
	 * @param dureeReccurence
	 * @param ecartReccurence
	 * @param prestations
	 * @param libelleReccurence
	 * @param libelleDureeReccurence
	 */
	public ReccurenceEntity(int idReccurence, int dureeReccurence, int ecartReccurence,
			List<PrestationEntity> prestations, LibelleReccurenceEntity libelleReccurence,
			LibelleDureeReccurenceEntity libelleDureeReccurence)
	{
		super();
		this.id = idReccurence;
		this.dureeReccurence = dureeReccurence;
		this.ecartReccurence = ecartReccurence;
		this.prestations = prestations;
		this.libelleReccurence = libelleReccurence;
		this.libelleDureeReccurence = libelleDureeReccurence;
	}
	public ReccurenceEntity()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdReccurence() {
		return id;
	}
	public void setIdReccurence(int idReccurence) {
		this.id = idReccurence;
	}
	public int getDureeReccurence() {
		return dureeReccurence;
	}
	public void setDureeReccurence(int dureeReccurence) {
		this.dureeReccurence = dureeReccurence;
	}
	public int getEcartReccurence() {
		return ecartReccurence;
	}
	public void setEcartReccurence(int ecartReccurence) {
		this.ecartReccurence = ecartReccurence;
	}
	public List<PrestationEntity> getPrestations() {
		return prestations;
	}
	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestations = prestations;
	}
	public LibelleReccurenceEntity getLibelleReccurence() {
		return libelleReccurence;
	}
	public void setLibelleReccurence(LibelleReccurenceEntity libelleReccurence) {
		this.libelleReccurence = libelleReccurence;
	}
	public LibelleDureeReccurenceEntity getLibelleDureeReccurence() {
		return libelleDureeReccurence;
	}
	public void setLibelleDureeReccurence(LibelleDureeReccurenceEntity libelleDureeReccurence) {
		this.libelleDureeReccurence = libelleDureeReccurence;
	}
	
	
	
}
