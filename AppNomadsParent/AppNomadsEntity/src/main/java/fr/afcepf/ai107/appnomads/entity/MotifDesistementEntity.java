package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "motif_desistement")
public class MotifDesistementEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer motifDesistement;
	@Column(name = "libelle_motif")
	private String libelleMotif;
	
	@OneToMany (mappedBy = "motifDesistementEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<InscriptionEntity> inscriptionEntity;
	
	public MotifDesistementEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MotifDesistementEntity(Integer motifDesistement, String libelleMotif) {
		super();
		this.motifDesistement = motifDesistement;
		this.libelleMotif = libelleMotif;
	}

	public Integer getMotifDesistement() {
		return motifDesistement;
	}

	public void setMotifDesistement(Integer motifDesistement) {
		this.motifDesistement = motifDesistement;
	}

	public String getLibelleMotif() {
		return libelleMotif;
	}

	public void setLibelleMotif(String libelleMotif) {
		this.libelleMotif = libelleMotif;
	}

	public static long getSerialVersionuid() {
		return serialVersionUID;
	}

	
}
