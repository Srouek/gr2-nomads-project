package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "preferences")
public class PreferencesEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "date_debut_choix")
	private Date dateDebutChoix;
	@Column(name = "heure_debut")
	private Date heureDebut;
	@Column(name = "heure_fin")
	private Date heureFin;
	@Column(name = "date_fin_choix")
	private Date dateFinChoix;
	@Column(name = "astreinte")
	private Boolean astreinte;
	@Column(name = "numero_du_jour")
	private int numeroDuJour;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseur;
	
	public PreferencesEntity(Integer id, Date dateDebutChoix, Date heureDebut, Date heureFin, Date dateFinChoix,
			Boolean astreinte, int numeroDuJour, MasseurEntity masseur) {
		super();
		this.id = id;
		this.dateDebutChoix = dateDebutChoix;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.dateFinChoix = dateFinChoix;
		this.astreinte = astreinte;
		this.numeroDuJour = numeroDuJour;
		this.masseur = masseur;
	}
	public PreferencesEntity(Integer id, Date dateDebutChoix, Date heureDebut, Date heureFin, Date dateFinChoix,
			Boolean astreinte, int numeroDuJour) {
		super();
		this.id = id;
		this.dateDebutChoix = dateDebutChoix;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.dateFinChoix = dateFinChoix;
		this.astreinte = astreinte;
		this.numeroDuJour = numeroDuJour;
	}
	public PreferencesEntity() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDateDebutChoix() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dateDebutChoix);
	}
	public void setDateDebutChoix(Date dateDebutChoix) {
		this.dateDebutChoix = dateDebutChoix;
	}
	public String getHeureDebut() {
		return new SimpleDateFormat("hh:mm").format(heureDebut);
	}
	public void setHeureDebut(Date heureDebut) {
		this.heureDebut = heureDebut;
	}
	public String getHeureFin() {
		return new SimpleDateFormat("hh:mm").format(heureFin);
	}
	public void setHeureFin(Date heureFin) {
		this.heureFin = heureFin;
	}
	public String getDateFinChoix() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dateFinChoix);
	}
	public void setDateFinChoix(Date dateFinChoix) {
		this.dateFinChoix = dateFinChoix;
	}
	public String getAstreinte() {
		return astreinte? "Oui" : "Non";
	}
	public void setAstreinte(Boolean astreinte) {
		this.astreinte = astreinte;
	}
	public String getNumeroDuJour() {
		return new SimpleDateFormat("EEEE").format(numeroDuJour);
	}
	public void setNumeroDuJour(int numeroDuJour) {
		this.numeroDuJour = numeroDuJour;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public MasseurEntity getMasseur() {
		return masseur;
	}
	public void setMasseur(MasseurEntity masseur) {
		this.masseur = masseur;
	}

}
