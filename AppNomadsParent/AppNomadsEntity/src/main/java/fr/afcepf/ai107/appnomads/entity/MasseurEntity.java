package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "masseur")
@PrimaryKeyJoinColumn(name="id")
public class MasseurEntity extends UtilisateurEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//abdel retirer @ID

	@Column(name = "nom")
	private String nom;
	@Column(name = "prenom")
	private String prenom;
	@Column(name = "date_de_naissance")
	private Date dateDeNaissance;
	@Column(name = "tel_principal")
	private String telPrincipal;
	@Column(name = "tel_secondaire")
	private String telSecondaire;
	@Column(name = "adresse_mail")
	private String adresseMail;
	@Column(name = "raison_sociale")
	private String raisonSociale;
	@Column(name = "siret")
	private String siret;
	@Column(name = "code_iban")
	private String codeIBAN;
	@Column(name = "code_bic")
	private String codeBIC;
	@Column(name = "date_embauche")
	private Date dateEmbauche;
	@Column(name = "photo")
	private String photo;
	@Column(name = "date_depart_definitif")
	private Date dateDepartDefinitif;
	
	

	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private GenreEntity genreEntity;
	
	@OneToMany (mappedBy = "masseur", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TransactionEntity> transactionEntity;
	

	@OneToMany (mappedBy = "masseurEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EmpruntMaterielEntity> empruntMaterielEntity;
	
	@OneToMany (mappedBy = "masseur", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PreferencesEntity> preferencesEntity;
	
	@OneToMany (mappedBy = "masseurEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EvolutionCarriereEntity> evolutionCarriereEntity;
	
	@OneToMany (mappedBy = "masseurEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<HistoriqueActiviteMasseurEntity> historiqueActiviteMasseurEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private IndisponibiliteMasseurEntity indisponibiliteMasseurEntity;

	@OneToMany( mappedBy = "masseurEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL) // EAGER Pour charger toutes les inscriptions du masseur au chargement du masseur
	private List<InscriptionEntity> inscriptionEntity;
	

	@OneToMany (mappedBy = "masseurEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL) // EAGER Pour charger tout les feeback du masseur au chargement du masseur
	private List<FeedbackMasseurEntity> feedbackMasseurEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private AdresseEntity adresseEntity;
	
	@OneToMany (mappedBy = "masseur", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PreferencesEntity> preferences;


	
	
	

	/**
	 * 
	 */
	public MasseurEntity()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param login
	 * @param password
	 */
	public MasseurEntity(Integer id, String login, String password, String role)
	{
		super(id, login, password,role);
		// TODO Auto-generated constructor stub
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getTelPrincipal() {
		return telPrincipal;
	}

	public void setTelPrincipal(String telPrincipal) {
		this.telPrincipal = telPrincipal;
	}

	public String getTelSecondaire() {
		return telSecondaire == null? telSecondaire : "";
	}

	public void setTelSecondaire(String telSecondaire) {
		this.telSecondaire = telSecondaire;
	}

	public String getAdresseMail() {
		return adresseMail;
	}

	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}

	public String getRaisonSociale() {
		return raisonSociale == null? raisonSociale : "";
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getSiret() {
		return siret == null? siret : "";
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public String getCodeIBAN() {
		return codeIBAN == null? codeIBAN : "";
	}

	public void setCodeIBAN(String codeIBAN) {
		this.codeIBAN = codeIBAN;
	}

	public String getCodeBIC() {
		return codeBIC == null? codeBIC : "";
	}

	public void setCodeBIC(String codeBIC) {
		this.codeBIC = codeBIC;
	}

	public Date getDateEmbauche() {
		return dateEmbauche;
	}

	public void setDateEmbauche(Date dateEmbauche) {
		this.dateEmbauche = dateEmbauche;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Date getDateDepartDefinitif() {
		return dateDepartDefinitif;
	}

	public void setDateDepartDefinitif(Date dateDepartDefinitif) {
		this.dateDepartDefinitif = dateDepartDefinitif;
	}

	public GenreEntity getGenreEntity() {
		return genreEntity;
	}

	public void setGenreEntity(GenreEntity genreEntity) {
		this.genreEntity = genreEntity;
	}

	public List<TransactionEntity> getTransactionEntity() {
		return transactionEntity;
	}

	public void setTransactionEntity(List<TransactionEntity> transactionEntity) {
		this.transactionEntity = transactionEntity;
	}

	public List<EmpruntMaterielEntity> getEmpruntMaterielEntity() {
		return empruntMaterielEntity;
	}

	public void setEmpruntMaterielEntity(List<EmpruntMaterielEntity> empruntMaterielEntity) {
		this.empruntMaterielEntity = empruntMaterielEntity;
	}

	public List<PreferencesEntity> getPreferencesEntity() {
		return preferencesEntity;
	}

	public void setPreferencesEntity(List<PreferencesEntity> preferencesEntity) {
		this.preferencesEntity = preferencesEntity;
	}

	public List<EvolutionCarriereEntity> getEvolutionCarriereEntity() {
		return evolutionCarriereEntity;
	}

	public void setEvolutionCarriereEntity(List<EvolutionCarriereEntity> evolutionCarriereEntity) {
		this.evolutionCarriereEntity = evolutionCarriereEntity;
	}

	public List<HistoriqueActiviteMasseurEntity> getHistoriqueActiviteMasseurEntity() {
		return historiqueActiviteMasseurEntity;
	}

	public void setHistoriqueActiviteMasseurEntity(List<HistoriqueActiviteMasseurEntity> historiqueActiviteMasseurEntity) {
		this.historiqueActiviteMasseurEntity = historiqueActiviteMasseurEntity;
	}

	public IndisponibiliteMasseurEntity getIndisponibiliteMasseurEntity() {
		return indisponibiliteMasseurEntity;
	}

	public void setIndisponibiliteMasseurEntity(IndisponibiliteMasseurEntity indisponibiliteMasseurEntity) {
		this.indisponibiliteMasseurEntity = indisponibiliteMasseurEntity;
	}
	

	public AdresseEntity getAdresseEntity() {
		return adresseEntity;
	}

	public void setAdresseEntity(AdresseEntity adresseEntity) {
		this.adresseEntity = adresseEntity;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public List<PreferencesEntity> getPreferences() {
		return preferences;
	}

	public void setPreferences(List<PreferencesEntity> preferences) {
		this.preferences = preferences;
	}


	public List<FeedbackMasseurEntity> getFeedbackMasseurEntity() {
		return feedbackMasseurEntity;
	}

	public void setFeedbackMasseurEntity(List<FeedbackMasseurEntity> feedbackMasseurEntity) {
		this.feedbackMasseurEntity = feedbackMasseurEntity;
	}

	public void setInscriptionEntity(List<InscriptionEntity> inscriptionEntity) {
		this.inscriptionEntity = inscriptionEntity;
	}
	public List<InscriptionEntity> getInscriptionEntity() {
		return inscriptionEntity;
	}

	

	
	

}
