package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "type_paiement")
public class TypePaiementEntity implements Serializable
{	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "id", nullable = false)
	private Integer id;
	@Column (name = "libelle")
	private String Libelle;
	@OneToMany (mappedBy = "typePaiment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<PrestationEntity> prestations;

	/**
	 * @param typePaiement
	 * @param libelle
	 * @param prestations
	 */
	public TypePaiementEntity(int typePaiement, String libelle, List<PrestationEntity> prestations)
	{
		super();
		this.id = typePaiement;
		Libelle = libelle;
		this.prestations = prestations;
	}
	

	/**
	 * 
	 */
	public TypePaiementEntity()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	public int getTypePaiement() {
		return id;
	}

	public void setTypePaiement(int typePaiement) {
		this.id = typePaiement;
	}

	public String getLibelle() {
		return Libelle;
	}

	public void setLibelle(String libelle) {
		Libelle = libelle;
	}

	public List<PrestationEntity> getPrestations() {
		return prestations;
	}

	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestations = prestations;
	}
	
	@Override
	public String toString() {
		return "TypePaiementEntity [id=" + id + ", Libelle=" + Libelle + "]";
	}
	
}
