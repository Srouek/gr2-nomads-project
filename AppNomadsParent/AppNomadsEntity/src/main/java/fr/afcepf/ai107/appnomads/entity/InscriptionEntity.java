package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.ws.rs.ext.ParamConverter.Lazy;

@Entity
@Table(name = "inscription")
public class InscriptionEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idInscription;
	@Column(name = "date_inscription")
	private Date dateInscription;
	@Column(name = "date_desistement")
	private Date dateDesistement;
	@Column(name = "id_binome")
	private Integer idBinome;
	@Column(name = "admin_valid")
	private Boolean adminValid;
	

	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseurEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MotifDesistementEntity motifDesistementEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PrestationEntity prestationEntity;

	
	public InscriptionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InscriptionEntity(Integer idInscription, Date dateInscription, Date dateDesistement, Integer idBinome,
			Boolean adminValid) {
		super();
		this.idInscription = idInscription;
		this.dateInscription = dateInscription;
		this.dateDesistement = dateDesistement;
		this.idBinome = idBinome;
		this.adminValid = adminValid;
	}

	public Integer getIdInscription() {
		return idInscription;
	}

	public void setIdInscription(Integer idInscription) {
		this.idInscription = idInscription;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public Date getDateDesistement() {
		return dateDesistement;
	}

	public void setDateDesistement(Date dateDesistement) {
		this.dateDesistement = dateDesistement;
	}

	public Integer getIdBinome() {
		return idBinome;
	}

	public void setIdBinome(Integer idBinome) {
		this.idBinome = idBinome;
	}

	public Boolean getAdminValid() {
		return adminValid;
	}

	public void setAdminValid(Boolean adminValid) {
		this.adminValid = adminValid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MasseurEntity getMasseurEntity() {
		return masseurEntity;
	}

	public void setMasseurEntity(MasseurEntity masseurEntity) {
		this.masseurEntity = masseurEntity;
	}

	public MotifDesistementEntity getMotifDesistementEntity() {
		return motifDesistementEntity;
	}

	public void setMotifDesistementEntity(MotifDesistementEntity motifDesistementEntity) {
		this.motifDesistementEntity = motifDesistementEntity;
	}

	public PrestationEntity getPrestationEntity() {
		return prestationEntity;
	}

	public void setPrestationEntity(PrestationEntity prestationEntity) {
		this.prestationEntity = prestationEntity;
	}

	public InscriptionEntity(Integer idInscription, Date dateInscription, Date dateDesistement, Integer idBinome,
			Boolean adminValid, MasseurEntity masseurEntity, MotifDesistementEntity motifDesistementEntity,
			PrestationEntity prestationEntity) {
		super();
		this.idInscription = idInscription;
		this.dateInscription = dateInscription;
		this.dateDesistement = dateDesistement;
		this.idBinome = idBinome;
		this.adminValid = adminValid;
		this.masseurEntity = masseurEntity;
		this.motifDesistementEntity = motifDesistementEntity;
		this.prestationEntity = prestationEntity;
	}

}
