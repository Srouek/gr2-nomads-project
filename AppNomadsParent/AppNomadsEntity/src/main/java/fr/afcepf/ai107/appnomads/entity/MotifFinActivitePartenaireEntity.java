package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "motif_fin_activite_partenaire")
public class MotifFinActivitePartenaireEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idMotif;
	@Column(name = "libelle")
	private String libelle;
	
	//Abdel
	
	@OneToMany (mappedBy = "motifFinActivitePartenaireEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<HistoriqueActivitePartenaireEntity> historiqueActivitePartenaireEntity;
	
	//abdel
	
	public MotifFinActivitePartenaireEntity() {
		super();
	}

	public MotifFinActivitePartenaireEntity(Integer idMotif, String libelle) {
		super();
		this.idMotif = idMotif;
		this.libelle = libelle;
	}

	public Integer getIdMotif() {
		return idMotif;
	}

	public void setIdMotif(Integer idMotif) {
		this.idMotif = idMotif;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		

}
