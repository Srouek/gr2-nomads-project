package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "feedback_partenaire")
public class FeedbackPartenaireEntity implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idFeedbackPartenaire;
	@Column(name = "commentaire_partenaire")
	private String commentairePartenaire;
	@Column(name = "date_feedback")
	private Date dateFeedback;
	@Column(name = "note_relationnel")
	private Integer noteRelationnel;
	@Column(name = "note_ponctualite")
	private Integer notePonctualite;
	@Column(name = "note_qualite_massage")
	private Integer noteQualiteMassage;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PrestationEntity prestationEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PartenaireEntity partenaireEntity;
	
	
	public FeedbackPartenaireEntity() {
		super();
	}

	
	
	public FeedbackPartenaireEntity(Integer idFeedbackPartenaire, String commentairePartenaire, Date dateFeedback,
			Integer noteRelationnel, Integer notePonctualite, Integer noteQualiteMassage,
			PrestationEntity prestationEntity, PartenaireEntity partenaireEntity) {
		super();
		this.idFeedbackPartenaire = idFeedbackPartenaire;
		this.commentairePartenaire = commentairePartenaire;
		this.dateFeedback = dateFeedback;
		this.noteRelationnel = noteRelationnel;
		this.notePonctualite = notePonctualite;
		this.noteQualiteMassage = noteQualiteMassage;
		this.prestationEntity = prestationEntity;
		this.partenaireEntity = partenaireEntity;
	}





	public Integer getIdFeedbackPartenaire() {
		return idFeedbackPartenaire;
	}

	public void setIdFeedbackPartenaire(Integer idFeedbackPartenaire) {
		this.idFeedbackPartenaire = idFeedbackPartenaire;
	}

	public String getCommentairePartenaire() {
		return commentairePartenaire;
	}

	public void setCommentairePartenaire(String commentairePartenaire) {
		this.commentairePartenaire = commentairePartenaire;
	}

	public Date getDateFeedback() {
		return dateFeedback;
	}

	public void setDateFeedback(Date dateFeedback) {
		this.dateFeedback = dateFeedback;
	}

	public Integer getNoteRelationnel() {
		return noteRelationnel;
	}

	public void setNoteRelationnel(Integer noteRelationnel) {
		this.noteRelationnel = noteRelationnel;
	}

	public Integer getNotePonctualite() {
		return notePonctualite;
	}

	public void setNotePonctualite(Integer notePonctualite) {
		this.notePonctualite = notePonctualite;
	}

	public Integer getNoteQualiteMassage() {
		return noteQualiteMassage;
	}

	public void setNoteQualiteMassage(Integer noteQualiteMassage) {
		this.noteQualiteMassage = noteQualiteMassage;
	}

	public PrestationEntity getPrestationEntity() {
		return prestationEntity;
	}

	public void setPrestationEntity(PrestationEntity prestationEntity) {
		this.prestationEntity = prestationEntity;
	}



	public PartenaireEntity getPartenaireEntity() {
		return partenaireEntity;
	}



	public void setPartenaireEntity(PartenaireEntity partenaireEntity) {
		this.partenaireEntity = partenaireEntity;
	}

	



}
