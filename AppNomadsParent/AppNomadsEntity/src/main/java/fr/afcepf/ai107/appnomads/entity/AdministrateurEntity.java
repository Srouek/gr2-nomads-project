package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "administrateur")
@PrimaryKeyJoinColumn(name="id")
public class AdministrateurEntity extends UtilisateurEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//abdel retirer @ID

	@Column(name = "nom")
	private String nom;
	@Column(name = "prenom")
	private String prenom;
	@Column(name = "adresse_mail")
	private String adresseMail;
	@Column(name = "photo")
	private String photo;
	
	
	
	public AdministrateurEntity() {
		super();
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresseMail() {
		return adresseMail;
	}
	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public AdministrateurEntity( String nom, String prenom, String adresseMail, String photo) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresseMail = adresseMail;
		this.photo = photo;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}


	
	
}
