package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "genre")
public class GenreEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "libelle")
	private String libelle;
	
	@OneToMany (mappedBy = "genreEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MasseurEntity> masseurEntity;
	
	public GenreEntity(Integer id, String libelle, List<MasseurEntity> masseur) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.masseurEntity = masseur;
	}

	public GenreEntity(Integer id, String genre) {
		super();
		this.id = id;
		this.libelle = genre;
	}

	public GenreEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<MasseurEntity> getMasseur() {
		return masseurEntity;
	}

	public void setMasseur(List<MasseurEntity> masseur) {
		this.masseurEntity = masseur;
	}

	@Override
	public String toString() {
		return "GenreEntity [id=" + id + ", libelle=" + libelle + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + ((masseurEntity == null) ? 0 : masseurEntity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenreEntity other = (GenreEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (masseurEntity == null) {
			if (other.masseurEntity != null)
				return false;
		} else if (!masseurEntity.equals(other.masseurEntity))
			return false;
		return true;
	}

}
