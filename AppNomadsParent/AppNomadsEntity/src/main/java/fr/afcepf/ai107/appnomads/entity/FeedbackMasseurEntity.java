package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "feedback_masseur")
public class FeedbackMasseurEntity implements Serializable {
	

	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idFeedbackMasseur;
	@Column(name = "commentaire_masseur")
	private String commentaireMasseur;
	@Column(name = "chiffre_daffaire")
	private Float chiffreDaffaire;
	@Column(name = "nombre_masseur")
	private Integer nombreMasseur;
	@Column(name = "date_feedback")
	private Date dateFeedback;
	@Column(name = "note_accueil")
	private Integer noteAccueil;
	@Column(name = "note_ambiance")
	private Integer noteAmbiance;
	@Column(name = "note_clientele")
	private Integer noteClientele;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PrestationEntity prestationEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseurEntity;
	
	public FeedbackMasseurEntity() {
		super();
	}

	

	/**
	 * @param idFeedbackMasseur
	 * @param commentaireMasseur
	 * @param chiffreDaffaire
	 * @param nombreMasseur
	 * @param dateFeedback
	 * @param noteAccueil
	 * @param noteAmbiance
	 * @param noteClientele
	 * @param prestationEntity
	 * @param masseurEntity
	 */
	public FeedbackMasseurEntity(Integer idFeedbackMasseur, String commentaireMasseur, Float chiffreDaffaire,
			Integer nombreMasseur, Date dateFeedback, Integer noteAccueil, Integer noteAmbiance, Integer noteClientele,
			PrestationEntity prestationEntity, MasseurEntity masseurEntity)
	{
		super();
		this.idFeedbackMasseur = idFeedbackMasseur;
		this.commentaireMasseur = commentaireMasseur;
		this.chiffreDaffaire = chiffreDaffaire;
		this.nombreMasseur = nombreMasseur;
		this.dateFeedback = dateFeedback;
		this.noteAccueil = noteAccueil;
		this.noteAmbiance = noteAmbiance;
		this.noteClientele = noteClientele;
		this.prestationEntity = prestationEntity;
		this.masseurEntity = masseurEntity;
	}






	public Integer getIdFeedbackMasseur() {
		return idFeedbackMasseur;
	}

	public void setIdFeedbackMasseur(Integer idFeedbackMasseur) {
		this.idFeedbackMasseur = idFeedbackMasseur;
	}

	public String getCommentaireMasseur() {
		return commentaireMasseur;
	}

	public void setCommentaireMasseur(String commentaireMasseur) {
		this.commentaireMasseur = commentaireMasseur;
	}

	public Float getChiffreDaffaire() {
		return chiffreDaffaire;
	}

	public void setChiffreDaffaire(Float chiffreDaffaire) {
		this.chiffreDaffaire = chiffreDaffaire;
	}

	public Integer getNombreMasseur() {
		return nombreMasseur;
	}

	public void setNombreMasseur(Integer nombreMasseur) {
		this.nombreMasseur = nombreMasseur;
	}

	public Date getDateFeedback() {
		return dateFeedback;
	}

	public void setDateFeedback(Date dateFeedback) {
		this.dateFeedback = dateFeedback;
	}

	public Integer getNoteAccueil() {
		return noteAccueil;
	}

	public void setNoteAccueil(Integer noteAccueil) {
		this.noteAccueil = noteAccueil;
	}

	public Integer getNoteAmbiance() {
		return noteAmbiance;
	}

	public void setNoteAmbiance(Integer noteAmbiance) {
		this.noteAmbiance = noteAmbiance;
	}

	public Integer getNoteClientele() {
		return noteClientele;
	}

	public void setNoteClientele(Integer noteClientele) {
		this.noteClientele = noteClientele;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public PrestationEntity getPrestationEntity() {
		return prestationEntity;
	}



	public void setPrestationEntity(PrestationEntity prestationEntity) {
		this.prestationEntity = prestationEntity;
	}



	public MasseurEntity getMasseurEntity() {
		return masseurEntity;
	}



	public void setMasseurEntity(MasseurEntity masseurEntity) {
		this.masseurEntity = masseurEntity;
	}



	
	
	

}
