package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "indisponibilite_partenaire")
public class IndisponibilitePartenaireEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idIndisponibilite;
	@Column(name = "date_debut")
	private Date dateDebut;
	@Column(name = "date_fin")
	private Date dateFin;
	
	//Abdel
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PartenaireEntity partenaireEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MotifIndisponibilitePartenaireEntity motifIndisponibilitePartenaireEntity;
	
	//abdel
	
	public IndisponibilitePartenaireEntity() {
		super();
	}

	public IndisponibilitePartenaireEntity(Integer idIndisponibilite, Date dateDebut, Date dateFin) {
		super();
		this.idIndisponibilite = idIndisponibilite;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public Integer getIdIndisponibilite() {
		return idIndisponibilite;
	}

	public void setIdIndisponibilite(Integer idIndisponibilite) {
		this.idIndisponibilite = idIndisponibilite;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
