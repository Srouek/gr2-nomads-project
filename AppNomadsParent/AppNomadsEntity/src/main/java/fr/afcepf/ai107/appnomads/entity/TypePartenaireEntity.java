package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "type_partenaire")
public class TypePartenaireEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idTypePartenaire;
	@Column(name = "libelle")
	private String libelle;
	
	
	//abdel
	@OneToMany (mappedBy = "typePartenaireEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PartenaireEntity> partenaireEntity;
	
	//abdel
	
	
	public TypePartenaireEntity() {
		super();
	}

	public TypePartenaireEntity(Integer idTypePartenaire, String libelle) {
		super();
		this.idTypePartenaire = idTypePartenaire;
		this.libelle = libelle;
	}

	public Integer getIdTypePartenaire() {
		return idTypePartenaire;
	}

	public void setIdTypePartenaire(Integer idTypePartenaire) {
		this.idTypePartenaire = idTypePartenaire;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}


