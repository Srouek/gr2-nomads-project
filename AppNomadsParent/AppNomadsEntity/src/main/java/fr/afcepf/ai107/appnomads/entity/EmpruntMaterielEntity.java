package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table (name = "emprunt_materiel")
public class EmpruntMaterielEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "quantite")
	private int quantite;
	@Column(name = "date_debut_emprunt")
	private Date dateDebutEmprunt;
	@Column(name = "date_fin_emprunt_prevue")
	private Date dateFinEmpruntPrevue;
	@Column(name = "date_retour")
	private Date dateRetour;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private LibelleMaterielEntity libelleEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PrestationEntity prestationEntity;
	
//	
//	@OneToMany (mappedBy = "empruntMaterielEntity", fetch = FetchType.LAZY)
//	private List<MasseurEntity> masseurEntity;
//	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseurEntity;
	

	
	
	public EmpruntMaterielEntity(Integer id, int quantite, Date dateDebutEmprunt, Date dateFinEmpruntPrevue,
			Date dateRetour) {
		super();
		this.id = id;
		this.quantite = quantite;
		this.dateDebutEmprunt = dateDebutEmprunt;
		this.dateFinEmpruntPrevue = dateFinEmpruntPrevue;
		this.dateRetour = dateRetour;

	}
	public EmpruntMaterielEntity() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Date getDateRetour() {
		return dateRetour;
	}
	public void setDateRetour(Date dateRetour) {
		this.dateRetour = dateRetour;
	}
	public LibelleMaterielEntity getLibelleEntity() {
		return libelleEntity;
	}
	public void setLibelleEntity(LibelleMaterielEntity libelleEntity) {
		this.libelleEntity = libelleEntity;
	}
	public PrestationEntity getPrestationEntity() {
		return prestationEntity;
	}
	public void setPrestationEntity(PrestationEntity prestationEntity) {
		this.prestationEntity = prestationEntity;
	}
	public Date getDateDebutEmprunt() {
		return dateDebutEmprunt;
	}
	public Date getDateFinEmpruntPrevue() {
		return dateFinEmpruntPrevue;
	}
	public MasseurEntity getMasseurEntity() {
		return masseurEntity;
	}
	public void setMasseurEntity(MasseurEntity masseurEntity) {
		this.masseurEntity=masseurEntity;		
	}
	public void setDateDebutEmprunt(Date dateDebutEmpruntMateriel) {
this.dateDebutEmprunt=dateDebutEmpruntMateriel;		
	}
	public void setDateFinEmpruntPrevue(Date dateFinEmpruntMateriel) {
this.dateFinEmpruntPrevue=dateFinEmpruntMateriel;		
	}
	@Override
	public String toString() {
		return "EmpruntMaterielEntity [id=" + id + ", quantite=" + quantite + ", dateDebutEmprunt=" + dateDebutEmprunt
				+ ", dateFinEmpruntPrevue=" + dateFinEmpruntPrevue + ", dateRetour=" + dateRetour + ", libelleEntity="
				+ libelleEntity + ", prestationEntity=" + prestationEntity + ", masseurEntity=" + masseurEntity + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateDebutEmprunt == null) ? 0 : dateDebutEmprunt.hashCode());
		result = prime * result + ((dateFinEmpruntPrevue == null) ? 0 : dateFinEmpruntPrevue.hashCode());
		result = prime * result + ((dateRetour == null) ? 0 : dateRetour.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libelleEntity == null) ? 0 : libelleEntity.hashCode());
		result = prime * result + ((masseurEntity == null) ? 0 : masseurEntity.hashCode());
		result = prime * result + ((prestationEntity == null) ? 0 : prestationEntity.hashCode());
		result = prime * result + quantite;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpruntMaterielEntity other = (EmpruntMaterielEntity) obj;
		if (dateDebutEmprunt == null) {
			if (other.dateDebutEmprunt != null)
				return false;
		} else if (!dateDebutEmprunt.equals(other.dateDebutEmprunt))
			return false;
		if (dateFinEmpruntPrevue == null) {
			if (other.dateFinEmpruntPrevue != null)
				return false;
		} else if (!dateFinEmpruntPrevue.equals(other.dateFinEmpruntPrevue))
			return false;
		if (dateRetour == null) {
			if (other.dateRetour != null)
				return false;
		} else if (!dateRetour.equals(other.dateRetour))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libelleEntity == null) {
			if (other.libelleEntity != null)
				return false;
		} else if (!libelleEntity.equals(other.libelleEntity))
			return false;
		if (masseurEntity == null) {
			if (other.masseurEntity != null)
				return false;
		} else if (!masseurEntity.equals(other.masseurEntity))
			return false;
		if (prestationEntity == null) {
			if (other.prestationEntity != null)
				return false;
		} else if (!prestationEntity.equals(other.prestationEntity))
			return false;
		if (quantite != other.quantite)
			return false;
		return true;
	}

	

}
