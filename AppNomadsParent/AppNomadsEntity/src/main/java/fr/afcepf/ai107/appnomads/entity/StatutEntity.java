package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="statut")
public class StatutEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "libelle")
	private String libelle;
	
	@OneToMany (mappedBy = "statutEntity",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<EvolutionCarriereEntity> evolutionCarriere;

	public StatutEntity(Integer id, String libelle, List<EvolutionCarriereEntity> evolutionCarriere) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.evolutionCarriere = evolutionCarriere;
	}

	public StatutEntity(Integer id, List<EvolutionCarriereEntity> evolutionCarriere) {
		super();
		this.id = id;
		this.evolutionCarriere = evolutionCarriere;
	}

	public StatutEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<EvolutionCarriereEntity> getEvolutionCarriere() {
		return evolutionCarriere;
	}

	public void setEvolutionCarriere(List<EvolutionCarriereEntity> evolutionCarriere) {
		this.evolutionCarriere = evolutionCarriere;
	}

}
