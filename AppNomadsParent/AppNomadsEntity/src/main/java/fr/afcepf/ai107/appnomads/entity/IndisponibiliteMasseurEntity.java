package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "indisponibilite")
public class IndisponibiliteMasseurEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "date_debut")
	private Date dateDebut;
	@Column(name = "date_fin")
	private Date dateFin;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MotifIndisponibiliteMasseurEntity motif;
	
	@OneToMany (mappedBy = "indisponibiliteMasseurEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MasseurEntity> masseur;
	
	public IndisponibiliteMasseurEntity(Integer id, Date dateDebut, Date dateFin,
			MotifIndisponibiliteMasseurEntity motif, List<MasseurEntity> masseur) {
		super();
		this.id = id;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.motif = motif;
		this.masseur = masseur;
	}

	public IndisponibiliteMasseurEntity(Integer id, Date dateDebut, Date dateFin, MotifIndisponibiliteMasseurEntity motif) {
		super();
		this.id = id;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.motif = motif;
	}

	public IndisponibiliteMasseurEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public MotifIndisponibiliteMasseurEntity getMotif() {
		return motif;
	}

	public void setMotif(MotifIndisponibiliteMasseurEntity motif) {
		this.motif = motif;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<MasseurEntity> getMasseur() {
		return masseur;
	}

	public void setMasseur(List<MasseurEntity> masseur) {
		this.masseur = masseur;
	}

}
