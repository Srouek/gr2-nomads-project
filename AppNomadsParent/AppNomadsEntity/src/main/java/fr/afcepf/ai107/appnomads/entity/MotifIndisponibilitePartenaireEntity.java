package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "motif_indisponibilite_partenaire")
public class MotifIndisponibilitePartenaireEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer motif;
	@Column(name = "libelle")
	private String libelle;
	
	//abdel
	
	@OneToMany (mappedBy = "motifIndisponibilitePartenaireEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<IndisponibilitePartenaireEntity> indisponibilitePartenaireEntity;
	//abdel
	
	
	public MotifIndisponibilitePartenaireEntity() {
		super();
	}

	public MotifIndisponibilitePartenaireEntity(Integer motif, String libelle) {
		super();
		this.motif = motif;
		this.libelle = libelle;
	}

	public Integer getMotif() {
		return motif;
	}

	public void setMotif(Integer motif) {
		this.motif = motif;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
