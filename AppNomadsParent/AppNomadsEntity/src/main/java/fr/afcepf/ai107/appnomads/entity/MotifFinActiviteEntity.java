package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "motif_fin_activite")
public class MotifFinActiviteEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "motif")
	private String motif;
	
	@OneToMany (mappedBy = "motif", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<HistoriqueActiviteMasseurEntity> historiqueActiviteMasseur;
	
	public MotifFinActiviteEntity(Integer id, String motif,
			List<HistoriqueActiviteMasseurEntity> historiqueActiviteMasseur) {
		super();
		this.id = id;
		this.motif = motif;
		this.historiqueActiviteMasseur = historiqueActiviteMasseur;
	}

	public MotifFinActiviteEntity(Integer id, String motif) {
		super();
		this.id = id;
		this.motif = motif;
	}

	public MotifFinActiviteEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMotif() {
		return motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<HistoriqueActiviteMasseurEntity> getHistoriqueActiviteMasseur() {
		return historiqueActiviteMasseur;
	}

	public void setHistoriqueActiviteMasseur(List<HistoriqueActiviteMasseurEntity> historiqueActiviteMasseur) {
		this.historiqueActiviteMasseur = historiqueActiviteMasseur;
	}

}
