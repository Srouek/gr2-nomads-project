package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Table (name = "utilisateur")
@Inheritance(strategy = InheritanceType.JOINED)
public class UtilisateurEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	@Column (name = "role")
	private String role;
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public UtilisateurEntity() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	

	/**
	 * @param id
	 * @param login
	 * @param password
	 */
	public UtilisateurEntity(Integer id, String login, String password, String role)
	{
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.role = role;
		
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
