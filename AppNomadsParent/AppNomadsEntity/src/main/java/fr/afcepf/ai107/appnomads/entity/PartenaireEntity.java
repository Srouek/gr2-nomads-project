package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "partenaire")
@PrimaryKeyJoinColumn(name="id")
public class PartenaireEntity extends UtilisateurEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//abdel retirer @ID


	@Column(name = "nom_courant")
	private String nomCourant;
	@Column(name = "raison_sociale")
	private String raisonSociale;
	@Column(name = "n_siret")
	private String numeroSiret;
	@Column(name = "n_tva_intracommunautaire")
	private String ntvaIntracommunautaire;
	@Column(name = "code_iban")
	private String codeIban;
	@Column(name = "code_bic")
	private String codeBic;
	@Column(name = "date_creation_partenariat")
	private Date dateCreationPartenaire;
	@Column(name = "tel_principal")
	private String telephonePrincipal;
	@Column(name = "personne_contact")
	private String personneContact;
	@Column(name = "tel_secondaire")
	private String telephoneSecondaire;
	@Column(name = "adresse_mail_personne_contact")
	private String adresseMailPersonneContact;
	@Column(name = "photo")
	private String photo;
	

	
	@OneToMany (mappedBy = "partenaireEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<HistoriqueActivitePartenaireEntity> historiqueActivitePartenaireEntity;
	
	@OneToMany (mappedBy = "partenaireEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<IndisponibilitePartenaireEntity> indisponibilitePartenaireEntity;
	
	@OneToMany (mappedBy = "partenaireEntity", fetch = FetchType.EAGER, cascade = CascadeType.ALL) // EAGER Pour charger toutes les prestations du partenaire au chargement du partenaire
	private List<PrestationEntity> prestationEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private TypePartenaireEntity typePartenaireEntity;
	
	@OneToMany (mappedBy = "partenaireEntity", fetch = FetchType.EAGER , cascade = CascadeType.ALL) // EAGER Pour charger tout les feeback du partenaire au chargement du partenaire
	private List<FeedbackPartenaireEntity> feedbackPartenaireEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private AdresseEntity adresseEntity;
	

	
	public PartenaireEntity() {
		super();
	}



	public PartenaireEntity(String nomCourant, String raisonSociale, String numeroSiret, String ntvaIntracommunautaire,
			String codeIban, String codeBic, Date dateCreationPartenaire, String telephonePrincipal,
			String personneContact, String telephoneSecondaire, String adresseMailPersonneContact, String photo,
			List<HistoriqueActivitePartenaireEntity> historiqueActivitePartenaireEntity,
			List<IndisponibilitePartenaireEntity> indisponibilitePartenaireEntity,
			List<PrestationEntity> prestationEntity, TypePartenaireEntity typePartenaireEntity,
			List<FeedbackPartenaireEntity> feedbackPartenaireEntity, AdresseEntity adresseEntity) {
		super();
		this.nomCourant = nomCourant;
		this.raisonSociale = raisonSociale;
		this.numeroSiret = numeroSiret;
		this.ntvaIntracommunautaire = ntvaIntracommunautaire;
		this.codeIban = codeIban;
		this.codeBic = codeBic;
		this.dateCreationPartenaire = dateCreationPartenaire;
		this.telephonePrincipal = telephonePrincipal;
		this.telephoneSecondaire = telephoneSecondaire;
		this.adresseMailPersonneContact = adresseMailPersonneContact;
		this.photo = photo;
		this.historiqueActivitePartenaireEntity = historiqueActivitePartenaireEntity;
		this.indisponibilitePartenaireEntity = indisponibilitePartenaireEntity;
		this.prestationEntity = prestationEntity;
		this.typePartenaireEntity = typePartenaireEntity;
		this.feedbackPartenaireEntity = feedbackPartenaireEntity;
		this.adresseEntity = adresseEntity;
	}



	public String getNomCourant() {
		return nomCourant;
	}



	public void setNomCourant(String nomCourant) {
		this.nomCourant = nomCourant;
	}



	public String getRaisonSociale() {
		return raisonSociale;
	}



	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}



	public String getNumeroSiret() {
		return numeroSiret;
	}



	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}



	public String getNtvaIntracommunautaire() {
		return ntvaIntracommunautaire;
	}



	public void setNtvaIntracommunautaire(String ntvaIntracommunautaire) {
		this.ntvaIntracommunautaire = ntvaIntracommunautaire;
	}



	public String getCodeIban() {
		return codeIban;
	}



	public void setCodeIban(String codeIban) {
		this.codeIban = codeIban;
	}



	public String getCodeBic() {
		return codeBic;
	}



	public void setCodeBic(String codeBic) {
		this.codeBic = codeBic;
	}



	public Date getDateCreationPartenaire() {
		return dateCreationPartenaire;
	}



	public void setDateCreationPartenaire(Date dateCreationPartenaire) {
		this.dateCreationPartenaire = dateCreationPartenaire;
	}



	public String getTelephonePrincipal() {
		return telephonePrincipal;
	}



	public void setTelephonePrincipal(String telephonePrincipal) {
		this.telephonePrincipal = telephonePrincipal;
	}



	public String getPersonneContact() {
		return personneContact;
	}



	public void setPersonneContact(String personneContact) {
		this.personneContact = personneContact;
	}



	public String getTelephoneSecondaire() {
		return telephoneSecondaire;
	}



	public void setTelephoneSecondaire(String telephoneSecondaire) {
		this.telephoneSecondaire = telephoneSecondaire;
	}



	public String getAdresseMailPersonneContact() {
		return adresseMailPersonneContact;
	}



	public void setAdresseMailPersonneContact(String adresseMailPersonneContact) {
		this.adresseMailPersonneContact = adresseMailPersonneContact;
	}



	public String getPhoto() {
		return photo;
	}



	public void setPhoto(String photo) {
		this.photo = photo;
	}



	public List<HistoriqueActivitePartenaireEntity> getHistoriqueActivitePartenaireEntity() {
		return historiqueActivitePartenaireEntity;
	}



	public void setHistoriqueActivitePartenaireEntity(
			List<HistoriqueActivitePartenaireEntity> historiqueActivitePartenaireEntity) {
		this.historiqueActivitePartenaireEntity = historiqueActivitePartenaireEntity;
	}



	public List<IndisponibilitePartenaireEntity> getIndisponibilitePartenaireEntity() {
		return indisponibilitePartenaireEntity;
	}



	public void setIndisponibilitePartenaireEntity(List<IndisponibilitePartenaireEntity> indisponibilitePartenaireEntity) {
		this.indisponibilitePartenaireEntity = indisponibilitePartenaireEntity;
	}



	public List<PrestationEntity> getPrestationEntity() {
		return prestationEntity;
	}



	public void setPrestationEntity(List<PrestationEntity> prestationEntity) {
		this.prestationEntity = prestationEntity;
	}



	public TypePartenaireEntity getTypePartenaireEntity() {
		return typePartenaireEntity;
	}



	public void setTypePartenaireEntity(TypePartenaireEntity typePartenaireEntity) {
		this.typePartenaireEntity = typePartenaireEntity;
	}



	public List<FeedbackPartenaireEntity> getFeedbackPartenaireEntity() {
		return feedbackPartenaireEntity;
	}



	public void setFeedbackPartenaireEntity(List<FeedbackPartenaireEntity> feedbackPartenaireEntity) {
		this.feedbackPartenaireEntity = feedbackPartenaireEntity;
	}



	public AdresseEntity getAdresseEntity() {
		return adresseEntity;
	}



	public void setAdresseEntity(AdresseEntity adresseEntity) {
		this.adresseEntity = adresseEntity;
	}



	@Override
	public String toString() {
		return "PartenaireEntity [nomCourant=" + nomCourant + ", raisonSociale=" + raisonSociale + ", numeroSiret="
				+ numeroSiret + ", ntvaIntracommunautaire=" + ntvaIntracommunautaire + ", codeIban=" + codeIban
				+ ", codeBic=" + codeBic + ", dateCreationPartenaire=" + dateCreationPartenaire
				+ ", telephonePrincipal=" + telephonePrincipal + ", personneContact=" + personneContact
				+ ", telephoneSecondaire=" + telephoneSecondaire + ", adresseMailPersonneContact="
				+ adresseMailPersonneContact + ", photo=" + photo + "]";
	}





	



	
	
	
}
