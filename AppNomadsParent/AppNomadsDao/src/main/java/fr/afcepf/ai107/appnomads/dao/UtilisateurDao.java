package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomads.idao.UtilisateurIDao;




@Remote(UtilisateurIDao.class)
@Stateless
public class UtilisateurDao extends GenericDao<UtilisateurEntity> implements UtilisateurIDao{
	
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	
	@SuppressWarnings("unchecked")
	@Override
	public UtilisateurEntity authenticate (String login, String password)  {
		Query query = em.createQuery("SELECT u FROM UtilisateurEntity u WHERE u.login = :paramlogin AND u.password = :parampassword");
		query.setParameter("paramlogin", login);
		query.setParameter("parampassword", password);
		List<UtilisateurEntity> utilisateurs = query.getResultList();
		UtilisateurEntity utilisateur = null;
		if (utilisateurs.size()>0){
			utilisateur = utilisateurs.get(0);
		}
		return utilisateur;
	}


	
	
}
