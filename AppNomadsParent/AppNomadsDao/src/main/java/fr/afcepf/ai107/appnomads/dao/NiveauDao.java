package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.NiveauEntity;
import fr.afcepf.ai107.appnomads.idao.NiveauIDao;

@Remote(NiveauIDao.class)
@Stateless
public class NiveauDao extends GenericDao<NiveauEntity> implements NiveauIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	public NiveauDao() {
	}

	@Override
	public NiveauEntity getNiveauByMasseur(MasseurEntity selectedMasseur) {
		NiveauEntity result= null;
		Query query = em.createQuery("SELECT n FROM NiveauEntity n "
					                + "INNER JOIN FETCH n.evolutionCarriere e "
					                + "WHERE e.masseurEntity = :paramMasseur"); 
		query.setParameter("paramMasseur", selectedMasseur);
		result = (NiveauEntity) query.getSingleResult();
		return result;
	}

}
