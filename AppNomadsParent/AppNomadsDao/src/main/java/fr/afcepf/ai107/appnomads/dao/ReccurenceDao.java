package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai107.appnomads.entity.ReccurenceEntity;
import fr.afcepf.ai107.appnomads.idao.ReccurenceIDao;

@Remote (ReccurenceIDao.class)
@Stateless
public class ReccurenceDao extends GenericDao<ReccurenceEntity> implements ReccurenceIDao
{
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;


}
