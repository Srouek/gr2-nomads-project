package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.StatutEntity;
import fr.afcepf.ai107.appnomads.idao.StatutIDao;

@Remote(StatutIDao.class)
@Stateless
public class StatutDao extends GenericDao<StatutEntity> implements StatutIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	public StatutDao() {
	}

	@Override
	public StatutEntity getStatutByMasseur(MasseurEntity selectedMasseur) {
		StatutEntity result= null;
		Query query = em.createQuery("SELECT s FROM StatutEntity s "
                + "INNER JOIN FETCH s.evolutionCarriere e "
                + "WHERE e.masseurEntity = :paramMasseur"); 
		query.setParameter("paramMasseur", selectedMasseur);
		result = (StatutEntity) query.getSingleResult();
		return result;
	}

}
