package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.idao.HistoriqueActiviteIDao;
import fr.afcepf.ai107.appnomads.idao.HistoriqueActivitePartenaireIDao;

@Remote(HistoriqueActivitePartenaireIDao.class)
@Stateless
public class HistoriqueActivitePartenaireDao extends GenericDao<HistoriqueActivitePartenaireEntity> implements HistoriqueActivitePartenaireIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	@Override
	public List<HistoriqueActivitePartenaireEntity> getListeHistoriqueActiviteByPartenaire(
			PartenaireEntity selectedPartenaire) {
		List<HistoriqueActivitePartenaireEntity> liste = null;
		Query query = em.createQuery("SELECT h FROM HistoriqueActivitePartenaireEntity h WHERE h.partenaireEntity = :paramPartenaire"); 
		query.setParameter("paramPartenaire", selectedPartenaire);
		liste = query.getResultList();
		return liste;
	}

	@Override
	public HistoriqueActivitePartenaireEntity getCurrentActivityByPartenaire(PartenaireEntity selectedPartenaire) {
		List<HistoriqueActivitePartenaireEntity> liste = null;
		Query query = em.createQuery("SELECT h FROM HistoriqueActivitePartenaireEntity h WHERE h.partenaireEntity = :paramPartenaire and h.dateFinActivite = null"); 
		query.setParameter("paramPartenaire", selectedPartenaire);
		liste = query.getResultList();
		return liste.get(0);
	}

	
}
