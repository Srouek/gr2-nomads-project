package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import fr.afcepf.ai107.appnomads.entity.AdministrateurEntity;
import fr.afcepf.ai107.appnomads.idao.AdministrateurIDao;


@Remote(AdministrateurIDao.class)
@Stateless
public class AdministrateurDao extends GenericDao<AdministrateurEntity> implements AdministrateurIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	
}