package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.InscriptionEntity;
import fr.afcepf.ai107.appnomads.idao.InscriptionIDao;

@Remote(InscriptionIDao.class)
@Stateless
public class InscriptionDao extends GenericDao<InscriptionEntity> implements InscriptionIDao
{
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	@Override
	public List<InscriptionEntity> getListInscriptionByPrestationID(int id) {
		List<InscriptionEntity> inscriptions = null;
		Query query = em.createQuery("SELECT i FROM InscriptionEntity i WHERE i.prestationEntity.id = :paramIdPrestation");
		query.setParameter("paramIdPrestation", id);
		inscriptions = query.getResultList();
		return inscriptions;
	}

}
