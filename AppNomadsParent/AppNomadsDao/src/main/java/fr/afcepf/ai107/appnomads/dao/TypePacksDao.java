package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.entity.TypePacksEntity;
import fr.afcepf.ai107.appnomads.idao.TypePacksIDao;

@Remote (TypePacksIDao.class)
@Stateless
public class TypePacksDao extends GenericDao<TypePacksEntity> implements TypePacksIDao {

	public TypePacksDao() {
		// TODO Auto-generated constructor stub
	}

}
