package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.idao.AdresseIDao;

@Remote(AdresseIDao.class)
@Stateless
public class AdresseDao extends GenericDao<AdresseEntity> implements AdresseIDao {
	
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	@Override
	public int getLastId() {
		Query query = em.createQuery("Select Max(a.id) from AdresseEntity a");
		return (int) query.getSingleResult();
	}
	
	
}
