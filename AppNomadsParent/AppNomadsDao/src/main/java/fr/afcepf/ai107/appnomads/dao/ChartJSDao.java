package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.idao.ChartJSIDao;

public class ChartJSDao extends GenericDao<PrestationEntity> implements ChartJSIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	
	@Override
	public List<PrestationEntity> getListePrestationByMasseur(MasseurEntity masseur) {
		List<PrestationEntity> prestations = null;
		Query query = em.createQuery("SELECT p FROM PrestationEntity p "
				                   + "INNER JOIN FETCH p.inscriptionEntity i "
				                   + "WHERE i.masseurEntity = :paramMasseur"); 
		query.setParameter("paramMasseur", masseur);
		prestations = query.getResultList();
		return prestations;
	
	
}


	@Override
	public List<PrestationEntity> getListePrestationByPartenaire(PartenaireEntity partenaire) {
		// TODO Auto-generated method stub
		return null;
	}
}
