package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PreferencesEntity;
import fr.afcepf.ai107.appnomads.idao.PreferencesIDao;

@Remote(PreferencesIDao.class)
@Stateless
public class PreferencesDao extends GenericDao<PreferencesEntity> implements PreferencesIDao {
	
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	@Override
	public List<PreferencesEntity> getListePreferencesByMasseur(MasseurEntity masseur) {
		List<PreferencesEntity> preferences = null;
		Query query = em.createQuery("SELECT p FROM PreferencesEntity p WHERE p.masseur.id = :paramIdMasseur"); 
		query.setParameter("paramIdMasseur", masseur.getId());
		preferences = query.getResultList();
		return preferences;
	}

}
