package fr.afcepf.ai107.appnomads.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.idao.MasseurIDao;

@Remote(MasseurIDao.class)
@Stateless
public class MasseurDao extends GenericDao<MasseurEntity> implements MasseurIDao {

	
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public boolean isActif(MasseurEntity selectedMasseur) {
		boolean isActif = true;
		List<HistoriqueActiviteMasseurEntity> result;
		Query query = em.createQuery("SELECT a FROM HistoriqueActiviteMasseurEntity a "
                + "INNER JOIN FETCH a.masseurEntity m "
                + "WHERE (m = :paramMasseur) and ((a.dateDebutActivite < :paramDate) and (a.dateFinActivite = null)) "); 
		query.setParameter("paramMasseur", selectedMasseur);
		query.setParameter("paramDate", new Date());
		result = query.getResultList();
		if (result.size() == 0) {
			isActif = false;
		}
		return isActif;
	}


}
