package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.entity.EvolutionCarriereEntity;
import fr.afcepf.ai107.appnomads.idao.EvolutionCarriereIDao;

@Remote (EvolutionCarriereIDao.class)
@Stateless
public class EvolutionCarriereDao extends GenericDao<EvolutionCarriereEntity> implements EvolutionCarriereIDao {

	
}
