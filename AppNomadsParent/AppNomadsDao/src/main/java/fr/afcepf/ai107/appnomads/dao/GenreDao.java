package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai107.appnomads.entity.GenreEntity;
import fr.afcepf.ai107.appnomads.idao.GenreIDao;

@Remote(GenreIDao.class)
@Stateless
public class GenreDao extends GenericDao<GenreEntity> implements GenreIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	


}
