package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.EmpruntMaterielEntity;
import fr.afcepf.ai107.appnomads.idao.EmpruntMaterielIDao;


@Remote(EmpruntMaterielIDao.class)
@Stateless
public class EmpruntMaterielDao extends GenericDao<EmpruntMaterielEntity> implements EmpruntMaterielIDao {

	
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	
	@Override
	public List<EmpruntMaterielEntity> getListeEmpruntMaterielByMasseur1(int id) {
		List<EmpruntMaterielEntity> empruntMateriel = null;
		Query query = em.createQuery("SELECT e FROM EmpruntMaterielEntity e WHERE e.masseurEntity.id = :paramIdMasseur"); 
		query.setParameter("paramIdMasseur", id);
		empruntMateriel = query.getResultList();
		return empruntMateriel;
	}


}
