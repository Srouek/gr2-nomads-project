package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.entity.TypePartenaireEntity;
import fr.afcepf.ai107.appnomads.idao.TypePartenaireIDao;

@Remote(TypePartenaireIDao.class)
@Stateless
public class TypePartenaireDao extends GenericDao<TypePartenaireEntity > implements TypePartenaireIDao{

	
}
