package fr.afcepf.ai107.appnomds.ibusiness.prestation;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.Init;
import fr.afcepf.ai107.appnomads.entity.InscriptionEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.ReccurenceEntity;
import fr.afcepf.ai107.appnomads.entity.TypePaiementEntity;

public interface PrestationIBusiness
{
	PrestationEntity createPrestation(PrestationEntity prestation);
	AdresseEntity ajouterAdresse(AdresseEntity adresse);
	ReccurenceEntity ajouterReccurence(ReccurenceEntity reccurence);
	List<PrestationEntity> findAllPrestation();
	PrestationEntity findPrestation(int id);
	List<Init> findAllInit();
	List<PartenaireEntity> displayPartenaires();
	List<FeedbackMasseurEntity> displayFeedbackMasseurByPrestation(PrestationEntity prestation);
	List<FeedbackPartenaireEntity> displayFeedbackPartenaireByPrestation(PrestationEntity prestation);
	List<InscriptionEntity> displayInscriptionsAllOnPrestation(PrestationEntity prestation);
	List<TypePaiementEntity> displayTypePaiement();
	List<PrestationEntity> getPrestations(boolean passed, boolean today, boolean future );
	Integer getInscriptionValidOnPrestation(PrestationEntity prestion);
	int inscrireMasseur(MasseurEntity masseur,List<InscriptionEntity> liste ,PrestationEntity prestation);
	int nombreFeedbackTotalMasseur();
	int nombreFeedbackTotalPartenaire();
	int nbFeedbackMasseurByPrestation(PrestationEntity prestation);
	int nbFeedbackPartenaireByPrestation(PrestationEntity prestation);
	Integer masseurInscrit(PrestationEntity prestation);
	Integer masseurPreInscrit(PrestationEntity prestation);
	void deletePrestation(PrestationEntity selectedPrestation);
	void update(PrestationEntity selectedPrestation);
	

	

}
