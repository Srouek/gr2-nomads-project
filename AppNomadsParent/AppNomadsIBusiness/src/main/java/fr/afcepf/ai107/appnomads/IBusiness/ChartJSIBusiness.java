package fr.afcepf.ai107.appnomads.IBusiness;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;

public interface ChartJSIBusiness {


	public List<PrestationEntity> getListePrestationsByPartenaire(PartenaireEntity partenaire); 
	public Integer nbMasseur() ;
	public Integer nbPartenaire() ;
	public Integer nbPrestation();
	public Integer totalCA();
	
	
}
