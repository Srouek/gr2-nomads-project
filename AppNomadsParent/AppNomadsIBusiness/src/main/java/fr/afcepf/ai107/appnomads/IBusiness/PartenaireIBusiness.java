package fr.afcepf.ai107.appnomads.IBusiness;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.TypePartenaireEntity;

public interface PartenaireIBusiness {
	
	List<TypePartenaireEntity> displayAllTypePartenaire();
	PartenaireEntity addPartenaire(PartenaireEntity partenaire);
	AdresseEntity addAdresse(AdresseEntity adresseEntity);
	PartenaireEntity getPartenaireById(int i);
	List<PrestationEntity> getListePrestationsByPartenaire(PartenaireEntity partenaire);
	HistoriqueActivitePartenaireEntity addHistoriqueActivitePartenaire(HistoriqueActivitePartenaireEntity historiqueActivitePartenaire);
	List<FeedbackPartenaireEntity> getListFeedbackPartenaire();
	List<PartenaireEntity> findAll();
	List<PartenaireEntity> displayAllPartenaire();
	List<HistoriqueActivitePartenaireEntity> getListeHistoriqueActiviteByPartenaire(
			PartenaireEntity selectedPartenaire);
	void updatePartenaire(PartenaireEntity profilPartenaire);
	void desactivatePartenaire(PartenaireEntity selectedPartenaire);
 
}
