package fr.afcepf.ai107.appnomads.IBusiness.connexion;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.AdministrateurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;



public interface UtilisateurIBusiness {


	List<UtilisateurEntity> displayAllUtilisateur();


	UtilisateurEntity connexion(String login, String password);
	
	AdministrateurEntity getAdminById(int id);
	PartenaireEntity getPartenaireById(int id);
	MasseurEntity getMasseurById(int id);

}
