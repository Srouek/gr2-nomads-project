package fr.afcepf.ai107.appnomads.business;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.IBusiness.PartenaireIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.TypePartenaireEntity;
import fr.afcepf.ai107.appnomads.idao.AdresseIDao;
import fr.afcepf.ai107.appnomads.idao.FeedbackPartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.HistoriqueActivitePartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.PartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.PrestationIDao;
import fr.afcepf.ai107.appnomads.idao.TypePartenaireIDao;

@Remote (PartenaireIBusiness.class)
@Stateless
public class PartenaireBusiness implements PartenaireIBusiness  {
	
	@EJB
	private TypePartenaireIDao proxyTypePartenaireDao;
	
	@EJB
	private PartenaireIDao proxyPartenaireDao;
	
	@EJB
	private AdresseIDao proxyAdresseDao;
	
	@EJB
	private HistoriqueActivitePartenaireIDao proxyHistoriqueActivitePartenaireDao;
	
	@EJB
	private PrestationIDao proxyPrestationDao;
	
	@EJB
	private FeedbackPartenaireIDao proxyFeedbackPartenaireDao;
	
	@Override
	public List<TypePartenaireEntity> displayAllTypePartenaire(){
		return proxyTypePartenaireDao.findAll();
		
	}
	
	@Override
	public PartenaireEntity addPartenaire(PartenaireEntity partenaire) {
		return proxyPartenaireDao.add(partenaire);
	}
	
	@Override
	public List<PartenaireEntity> displayAllPartenaire(){
		return proxyPartenaireDao.findAll();
	}
	
	@Override
	public List<PartenaireEntity> findAll() {
		return proxyPartenaireDao.findAll();
	}
	
	@Override
	public AdresseEntity addAdresse(AdresseEntity adresse) {
		return proxyAdresseDao.add(adresse);
	}
	
	@Override
	public HistoriqueActivitePartenaireEntity addHistoriqueActivitePartenaire(HistoriqueActivitePartenaireEntity historiqueActivitePartenaire) {
			return proxyHistoriqueActivitePartenaireDao.add(historiqueActivitePartenaire);

	}
	
	
	
	@Override
	public PartenaireEntity getPartenaireById(int i) {
		return proxyPartenaireDao.getById(i);

	}
	@Override
	public List<PrestationEntity> getListePrestationsByPartenaire(PartenaireEntity partenaire) {
		return proxyPrestationDao.getListePrestationByPartenaire(partenaire);

	}

	public TypePartenaireIDao getProxyTypePartenaireDao() {
		return proxyTypePartenaireDao;
	}

	public void setProxyTypePartenaireDao(TypePartenaireIDao proxyTypePartenaireDao) {
		this.proxyTypePartenaireDao = proxyTypePartenaireDao;
	}

	public PartenaireIDao getProxyPartenaireDao() {
		return proxyPartenaireDao;
	}

	public void setProxyPartenaireDao(PartenaireIDao proxyPartenaireDao) {
		this.proxyPartenaireDao = proxyPartenaireDao;
	}

	public AdresseIDao getProxyAdresseDao() {
		return proxyAdresseDao;
	}

	public void setProxyAdresseDao(AdresseIDao proxyAdresseDao) {
		this.proxyAdresseDao = proxyAdresseDao;
	}

	public HistoriqueActivitePartenaireIDao getProxyHistoriqueActivitePartenaireDao() {
		return proxyHistoriqueActivitePartenaireDao;
	}

	public void setProxyHistoriqueActivitePartenaireDao(
			HistoriqueActivitePartenaireIDao proxyHistoriqueActivitePartenaireDao) {
		this.proxyHistoriqueActivitePartenaireDao = proxyHistoriqueActivitePartenaireDao;
	}

	public PrestationIDao getProxyPrestationDao() {
		return proxyPrestationDao;
	}

	public void setProxyPrestationDao(PrestationIDao proxyPrestationDao) {
		this.proxyPrestationDao = proxyPrestationDao;
	}

	public FeedbackPartenaireIDao getProxyFeedbackPartenaireDao() {
		return proxyFeedbackPartenaireDao;
	}

	public void setProxyFeedbackPartenaireDao(FeedbackPartenaireIDao proxyFeedbackPartenaireDao) {
		this.proxyFeedbackPartenaireDao = proxyFeedbackPartenaireDao;
	}


	@Override
	public List<FeedbackPartenaireEntity> getListFeedbackPartenaire() {
		return proxyFeedbackPartenaireDao.findAll();
	}

	@Override
	public List<HistoriqueActivitePartenaireEntity> getListeHistoriqueActiviteByPartenaire(
			PartenaireEntity selectedPartenaire) {
		return proxyHistoriqueActivitePartenaireDao.getListeHistoriqueActiviteByPartenaire(selectedPartenaire);
	}

	@Override
	public void updatePartenaire(PartenaireEntity profilPartenaire) {
		proxyPartenaireDao.update(profilPartenaire);
		
	}

	@Override
	public void desactivatePartenaire(PartenaireEntity selectedPartenaire) {
		HistoriqueActivitePartenaireEntity activite = proxyHistoriqueActivitePartenaireDao.getCurrentActivityByPartenaire(selectedPartenaire);
		activite.setDateFinActivite(new Date());
		proxyHistoriqueActivitePartenaireDao.update(activite);
		
	}

	
	}



	


