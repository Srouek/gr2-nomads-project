package fr.afcepf.ai107.appnomads.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.IBusiness.LocationMaterielIBusiness;
import fr.afcepf.ai107.appnomads.entity.EmpruntMaterielEntity;
import fr.afcepf.ai107.appnomads.entity.LibelleMaterielEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.idao.EmpruntMaterielIDao;
import fr.afcepf.ai107.appnomads.idao.LibelleMaterielIDao;

@Remote (LocationMaterielIBusiness.class)
@Stateless
public class LocationMaterielBusiness implements LocationMaterielIBusiness{

	@EJB
	private LibelleMaterielIDao proxyLibelleMaterielDao;
	
	@EJB
	private EmpruntMaterielIDao proxyEmpruntMaterielDao;

	@Override
	public List<LibelleMaterielEntity> displayAllMateriel(){
		return proxyLibelleMaterielDao.findAll();
	}

	
	
	@Override
	public EmpruntMaterielEntity addEmpruntMateriel (EmpruntMaterielEntity empruntMateriel) {
		return proxyEmpruntMaterielDao.add(empruntMateriel);
	}



	
	
}
