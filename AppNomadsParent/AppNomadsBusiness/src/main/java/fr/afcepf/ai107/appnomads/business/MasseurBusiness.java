package fr.afcepf.ai107.appnomads.business;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;


import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.EvolutionCarriereEntity;
import fr.afcepf.ai107.appnomads.entity.GenreEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.NiveauEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PreferencesEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.StatutEntity;
import fr.afcepf.ai107.appnomads.entity.TransactionEntity;
import fr.afcepf.ai107.appnomads.entity.TypePacksEntity;
import fr.afcepf.ai107.appnomads.idao.AdresseIDao;
import fr.afcepf.ai107.appnomads.idao.EvolutionCarriereIDao;
import fr.afcepf.ai107.appnomads.idao.GenreIDao;
import fr.afcepf.ai107.appnomads.idao.HistoriqueActiviteIDao;
import fr.afcepf.ai107.appnomads.idao.MasseurIDao;
import fr.afcepf.ai107.appnomads.idao.NiveauIDao;
import fr.afcepf.ai107.appnomads.idao.PreferencesIDao;
import fr.afcepf.ai107.appnomads.idao.PrestationIDao;
import fr.afcepf.ai107.appnomads.idao.StatutIDao;
import fr.afcepf.ai107.appnomads.idao.TransactionIDao;
import fr.afcepf.ai107.appnomads.idao.TypePacksIDao;

@Remote (MasseurIBusiness.class)
@Stateless
public class MasseurBusiness implements MasseurIBusiness {
	
	@EJB
	private GenreIDao proxyGenreDao;
	
	@EJB
	private MasseurIDao proxyMasseurDao;
	
	@EJB
	private AdresseIDao proxyAdresseDao;
	
	@EJB
	private EvolutionCarriereIDao proxyEvolutionCarriereDao;
	
	@EJB
	private HistoriqueActiviteIDao proxyHistoriqueActiviteDao;
	
	@EJB
	private PrestationIDao proxyPrestationDao;
	
	@EJB
	private PreferencesIDao proxyPreferencesDao;
	
	@EJB
	private NiveauIDao proxyNiveauDao;
	
	@EJB
	private StatutIDao proxyStatutDao;
	
	@EJB
	private TypePacksIDao proxyTypeDao;
	
	@EJB
	private TransactionIDao proxyTransactionDao;
	
	@Override
	public List<GenreEntity> displayAllGenre(){
		return proxyGenreDao.findAll();
		
	}

	@Override
	public MasseurEntity addMasseur(MasseurEntity masseur) {
		return proxyMasseurDao.add(masseur);
		
	}

	@Override
	public AdresseEntity addAdresse(AdresseEntity adresse) {
		return proxyAdresseDao.add(adresse);
		
	}

	@Override
	public EvolutionCarriereEntity addEvolutionCarriere(EvolutionCarriereEntity evolutionCarriere) {
		return proxyEvolutionCarriereDao.add(evolutionCarriere);
		
	}

	@Override
	public HistoriqueActiviteMasseurEntity addHistoriqueActivite(HistoriqueActiviteMasseurEntity historiqueActivite) {
		return proxyHistoriqueActiviteDao.add(historiqueActivite);
		
	}

	@Override
	public MasseurEntity getMasseurById(int i) {
		
		return proxyMasseurDao.getById(i);
	}

	@Override
	public List<PrestationEntity> getListePrestationsByMasseur(MasseurEntity masseur) {
		
		return proxyPrestationDao.getListePrestationByMasseur(masseur);
	}

	@Override
	public List<PreferencesEntity> getListePreferencesByMasseur(MasseurEntity masseur) {
		return proxyPreferencesDao.getListePreferencesByMasseur(masseur);
	};
	@Override
	public List<MasseurEntity> findAll() {
		return proxyMasseurDao.findAll();
	}

	@Override
	public StatutEntity getStatutByMasseur(MasseurEntity selectedMasseur) {
		return proxyStatutDao.getStatutByMasseur(selectedMasseur);
	}

	@Override
	public NiveauEntity getNiveauByMasseur(MasseurEntity selectedMasseur) {
		return proxyNiveauDao.getNiveauByMasseur(selectedMasseur);
	}

	@Override
	public boolean isActif(MasseurEntity selectedMasseur) {
		return proxyMasseurDao.isActif(selectedMasseur);
	}

	@Override
	public void updateMasseur(MasseurEntity selectedMasseur) {
		proxyMasseurDao.update(selectedMasseur);
		
	}
	@Override
	public List<HistoriqueActiviteMasseurEntity> getListeHistoriqueActiviteByMasseur(MasseurEntity selectedMasseur) {
		return proxyHistoriqueActiviteDao.getListeHistoriqueActiviteByMasseur(selectedMasseur);
	}

	@Override
	public Integer nbMasseurTotal() {
		return proxyMasseurDao.findAll().size();
	}

	public List<TypePacksEntity> getListeTypePacks() {
		
		return proxyTypeDao.findAll();

	}

	@Override
	public void addTransaction(TransactionEntity transaction) {
		proxyTransactionDao.add(transaction);
		
	}

	@Override
	public float getSoldeByMasseur(MasseurEntity utilisateur) {
		
		return proxyTransactionDao.getSoldeByMasseur(utilisateur);
	}

	@Override
	public void desactiverMasseur(MasseurEntity selectedMasseur) {
		HistoriqueActiviteMasseurEntity activite = proxyHistoriqueActiviteDao.getCurrentActiviteByMasseur(selectedMasseur);
		activite.setDateFinActivite(new Date());
		proxyHistoriqueActiviteDao.update(activite);
		
	}
	
	
}
