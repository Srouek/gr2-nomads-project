package fr.afcepf.ai107.appnomds.business.prestation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.Init;
import fr.afcepf.ai107.appnomads.entity.InscriptionEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.NiveauEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.ReccurenceEntity;
import fr.afcepf.ai107.appnomads.entity.StatutEntity;
import fr.afcepf.ai107.appnomads.entity.TransactionEntity;
import fr.afcepf.ai107.appnomads.entity.TypePaiementEntity;
import fr.afcepf.ai107.appnomads.idao.AdresseIDao;
import fr.afcepf.ai107.appnomads.idao.FeedbackMasseurIDao;
import fr.afcepf.ai107.appnomads.idao.FeedbackPartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.InitIDao;
import fr.afcepf.ai107.appnomads.idao.InscriptionIDao;
import fr.afcepf.ai107.appnomads.idao.MasseurIDao;
import fr.afcepf.ai107.appnomads.idao.NiveauIDao;
import fr.afcepf.ai107.appnomads.idao.PartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.PrestationIDao;
import fr.afcepf.ai107.appnomads.idao.ReccurenceIDao;
import fr.afcepf.ai107.appnomads.idao.StatutIDao;
import fr.afcepf.ai107.appnomads.idao.TransactionIDao;
import fr.afcepf.ai107.appnomads.idao.TypePaiementIDao;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@Remote(PrestationIBusiness.class)
@Stateless
public class PrestationBusiness implements PrestationIBusiness
{
	@EJB
	private PrestationIDao proxyPrestationDao;

	@EJB
	private InitIDao proxyInitDao;

	@EJB
	private PartenaireIDao proxyPartenaireDao;

	@EJB
	private FeedbackMasseurIDao proxyFeedbackMasseurDao;

	@EJB
	private FeedbackPartenaireIDao proxyFeedbackPartenaireDao;

	@EJB
	private MasseurIDao proxyMasseurDao;

	@EJB
	private InscriptionIDao proxyInscriptionDao;

	@EJB
	private NiveauIDao proxyNiveauDao;

	@EJB
	private StatutIDao proxyStatuDao;
	
	@EJB

	private AdresseIDao proxyAdressDao;
	
	@EJB
	private ReccurenceIDao proxyReccurenceDao;
	
	@EJB
	private TypePaiementIDao proxyTypaiementDao;

	private TransactionIDao proxyTransactionDao;


	@Override
	public PrestationEntity createPrestation(PrestationEntity prestation) {
		PrestationEntity prestationReturned = null;
		if (prestation != null)
		{
			prestationReturned  = proxyPrestationDao.add(prestation);
		}
		return prestationReturned;
	}

	@Override
	public List<PrestationEntity> findAllPrestation() {
		return proxyPrestationDao.findAll();
	}

	@Override
	public List<Init> findAllInit() {
		return proxyInitDao.findAll();
	}

	@Override
	public PrestationEntity findPrestation(int id) {
		return proxyPrestationDao.getById(id);
	}

	@Override
	public List<PartenaireEntity> displayPartenaires() {
		
		return proxyPartenaireDao.findAll();
	}

	
	

	public List<PrestationEntity> getPrestations(boolean passed, boolean today, boolean future) {
		List<PrestationEntity> prestations = proxyPrestationDao.findAll();
		List<PrestationEntity> resultat = new ArrayList<PrestationEntity>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date aujourdhui = null;
		try
		{
			aujourdhui = formatter.parse(formatter.format(new Date()));
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		if (passed && today && future)
		{
			resultat = prestations;
		} else if (passed | today | future)
		{
			for (PrestationEntity prestationEntity : prestations)
			{
				if (passed && (prestationEntity.getDatePrestationDate().before(aujourdhui)))
				{
					resultat.add(prestationEntity);
				}
				if (today && (prestationEntity.getDatePrestationDate().compareTo(aujourdhui)) == 0)
				{
					resultat.add(prestationEntity);
				}
				if (future && (prestationEntity.getDatePrestationDate().after(aujourdhui)))
				{
					resultat.add(prestationEntity);
				}

			}

		}
		return resultat;
	}

	@Override
	public List<FeedbackMasseurEntity> displayFeedbackMasseurByPrestation(PrestationEntity prestation) {
		// TODO Auto-generated method stub
		return proxyFeedbackMasseurDao.getListeFeedbackMasseurByPrestation(prestation.getIdPrestation());
	}

	@Override
	public List<FeedbackPartenaireEntity> displayFeedbackPartenaireByPrestation(PrestationEntity prestation) {
		// TODO Auto-generated method stub
		return proxyFeedbackPartenaireDao.getListeFeedbackPartenaireByPrestation(prestation.getIdPrestation());
	}

	@Override
	public List<InscriptionEntity> displayInscriptionsAllOnPrestation(PrestationEntity prestation) {
		// TODO Auto-generated method stub
		return proxyInscriptionDao.getListInscriptionByPrestationID(prestation.getIdPrestation());
	}

	@Override
	public Integer getInscriptionValidOnPrestation(PrestationEntity prestation) {
		List<InscriptionEntity> inscriptions = displayInscriptionsAllOnPrestation(prestation);
		List<InscriptionEntity> result = new ArrayList<InscriptionEntity>();

		for (InscriptionEntity inscriptionEntity : inscriptions)
		{
			if (inscriptionEntity.getDateDesistement() != null)
			{
				result.add(inscriptionEntity);
			}
		}
		return result.size();
	}

	// retourne -1 si pas de place, 0 si le masseur est préinscrit, 1 si le masseur, -2 solde insuffisant
	// est inscrit
	@SuppressWarnings("null")
	@Override
	public int inscrireMasseur(MasseurEntity masseur, List<InscriptionEntity> liste, PrestationEntity prestation) {
		int etat = -3;
		int idNeonomad = 1;
		int idFormateur = 1;
		int soldeInsuffisant = -2;
		int pasDePlace = -1;
		int masseurSeraPreinscrit = 0;
		int inscriptionvalidee = 1 ;
		MasseurEntity masseurPreinscrit;
		InscriptionEntity nouvelleInscription = null;
		nouvelleInscription = remplirDonnesInscription(nouvelleInscription, masseur, prestation);
		List<InscriptionEntity> listePreinscrit = null;
		List<InscriptionEntity> listeInscritConfirmee = null;
		InscriptionEntity inscriptionTrouvee = null;
		
		if (proxyTransactionDao.getSoldeByMasseur(masseur)>prestation.getCoutsInscription()) {
			
		
			// Chercher la liste des inscriptions confirmées et non confirmées
			for (InscriptionEntity inscriptionEntity : liste)
			{
				if (inscriptionEntity.getIdBinome() == null)
				{
					listePreinscrit.add(inscriptionEntity);
				} else
				{
					listeInscritConfirmee.add(inscriptionEntity);
				}
			}
			// cas 1: aucune inscription trouvée - première inscription
			if (listeInscritConfirmee == null && listePreinscrit == null)
			{
				// Persister une inscription
				etat = masseurSeraPreinscrit;
				proxyInscriptionDao.add(nouvelleInscription);
				//Payer la prestation
				payerInscription(masseur, prestation);
			} else if (listeInscritConfirmee.size() == prestation.getNbMasseur())
			{
				// Cas 2 : pas de places dispo
				etat = pasDePlace;
			} else
			{
				// recuperer la premiere inscription trouvée
				inscriptionTrouvee = listePreinscrit.get(0);
				masseurPreinscrit = proxyMasseurDao.getById(inscriptionTrouvee.getIdBinome());
				// Cas 3 : préinscription trouvée - verification du niveau nouveau masseur
				if (getNiveauByMasseur(masseur).getId() > idNeonomad)
				{
					// Cas 3.1 : nouveau masseur nomads
					if (getNiveauByMasseur(masseurPreinscrit).getId() > idNeonomad)
					{
						// Cas 3.1.1 : masseur préinscrit nomads
						etat = inscriptionvalidee;
						//Payer la prestation
						payerInscription(masseur, prestation);
						finaliserInscriptions(masseurPreinscrit, masseurPreinscrit, nouvelleInscription,
								inscriptionTrouvee);
					} else
					{
						// Cas 3.1.2 : masseur préinscrit néonomads - vérification du statut nouveau
						// masseur
						if (getStatutByMasseur(masseur).getId() == idFormateur)
						{
							// nouveau masseur formateur
							etat = inscriptionvalidee;
							//Payer la prestation
							payerInscription(masseur, prestation);
							finaliserInscriptions(masseurPreinscrit, masseurPreinscrit, nouvelleInscription,
									inscriptionTrouvee);
						} else
						{
							// nouveau masseur non formateur
							etat = masseurSeraPreinscrit;
							//Payer la prestation
							payerInscription(masseur, prestation);
							proxyInscriptionDao.add(nouvelleInscription);
						}
	
					}
				} else
				{
					// Cas 3.2 : nouveau masseur nenomads
					if (getNiveauByMasseur(masseurPreinscrit).getId() == idNeonomad)
					{
						// Cas 3.2.1 : masseur préinscrit nenomads
						// persister l'inscription
						etat = masseurSeraPreinscrit;
						proxyInscriptionDao.add(nouvelleInscription);
						//Payer la prestation
						payerInscription(masseur, prestation);
					} else
					{
						// Cas 3.2.2 : masseur préinscrit nomdas - vérification du statut nouveau
						// masseur
						if (getStatutByMasseur(masseurPreinscrit).getId() == idFormateur)
						{
							// masseur préinscrit formateur
							etat = inscriptionvalidee;
							finaliserInscriptions(masseurPreinscrit, masseurPreinscrit, nouvelleInscription,
									inscriptionTrouvee);
							//Payer la prestation
							payerInscription(masseur, prestation);
						} else
						{
	
							// masseur préinscrit non formateur
							// persister l'inscription
							etat = masseurSeraPreinscrit;
							proxyInscriptionDao.add(nouvelleInscription);
							//Payer la prestation
							payerInscription(masseur, prestation);
						}
					}
	
				}
			}
		}
		else {
			etat = soldeInsuffisant;
		}
		return etat;
	}

	private InscriptionEntity remplirDonnesInscription(InscriptionEntity nouvelleInscription, MasseurEntity masseur,
			PrestationEntity prestation) {


		nouvelleInscription.setDateInscription(new Date());
		nouvelleInscription.setMasseurEntity(masseur);
		nouvelleInscription.setPrestationEntity(prestation);
		return nouvelleInscription;
	}
	private void payerInscription(MasseurEntity masseur, PrestationEntity prestation) {

		TransactionEntity nouvelleTransaction = new TransactionEntity();
		int coutInscription = (int) ((-1) * prestation.getCoutsInscription()); 
		nouvelleTransaction.setDateAchat(new Date());
		nouvelleTransaction.setMasseur(masseur);
		nouvelleTransaction.setQuantitePack(coutInscription);
		proxyTransactionDao.add(nouvelleTransaction);
	}

	private NiveauEntity getNiveauByMasseur(MasseurEntity masseur) {
		return proxyNiveauDao.getNiveauByMasseur(masseur);
	}

	private StatutEntity getStatutByMasseur(MasseurEntity masseur) {
		return proxyStatuDao.getStatutByMasseur(masseur);
	}

	private void finaliserInscriptions(MasseurEntity masseurPreinscrit, MasseurEntity masseur,
			InscriptionEntity nouvelleInscription, InscriptionEntity inscriptionTrouvee) {
		nouvelleInscription.setIdBinome(masseurPreinscrit.getId());
		proxyInscriptionDao.add(nouvelleInscription);
		inscriptionTrouvee.setIdBinome(masseur.getId());
		proxyInscriptionDao.update(inscriptionTrouvee);
	}

	@Override
	public Integer masseurInscrit(PrestationEntity prestation) {
		List<InscriptionEntity> inscrits = displayInscriptionsAllOnPrestation(prestation);
		List<InscriptionEntity> result = new ArrayList<InscriptionEntity>();
		
		for (InscriptionEntity inscrition : inscrits)
		{
			if(inscrition.getDateDesistement() != null && inscrition.getIdBinome() != null) {
				result.add(inscrition);
			}
		}
		return result.size();
	}

	@Override
	public Integer masseurPreInscrit(PrestationEntity prestation) {
		List<InscriptionEntity> inscrits = displayInscriptionsAllOnPrestation(prestation);
		List<InscriptionEntity> result = new ArrayList<InscriptionEntity>();
		for (InscriptionEntity inscriptionEntity : inscrits)
		{
			if(inscriptionEntity.getDateDesistement() != null && inscriptionEntity.getIdBinome() == null) {
				result.add(inscriptionEntity);
			}
		}
		return result.size();
	}

	@Override
	public AdresseEntity ajouterAdresse(AdresseEntity adresse) {
		// TODO Auto-generated method stub
		return proxyAdressDao.add(adresse);
	}

	@Override
	public ReccurenceEntity ajouterReccurence(ReccurenceEntity reccurence) {
		// TODO Auto-generated method stub
		return proxyReccurenceDao.add(reccurence);
	}

	@Override
	public List<TypePaiementEntity> displayTypePaiement() {
		// TODO Auto-generated method stub
		return proxyTypaiementDao.findAll();
	}

	@Override
	public int nombreFeedbackTotalMasseur() {
		List<FeedbackMasseurEntity> totalFeedBack = proxyFeedbackMasseurDao.findAll();
		return totalFeedBack.size();
	}

	@Override
	public int nombreFeedbackTotalPartenaire() {
		List<FeedbackPartenaireEntity> totalFeedBack = proxyFeedbackPartenaireDao.findAll();
		return totalFeedBack.size();
	}

	@Override
	public int nbFeedbackMasseurByPrestation(PrestationEntity prestation) {
		List<FeedbackMasseurEntity> totalFeedBack = displayFeedbackMasseurByPrestation(prestation);
		return totalFeedBack.size();
	}

	@Override
	public int nbFeedbackPartenaireByPrestation(PrestationEntity prestation) {
		List<FeedbackPartenaireEntity> totalFeedBack = displayFeedbackPartenaireByPrestation(prestation);
		return totalFeedBack.size();
	}

	@Override
	public void deletePrestation(PrestationEntity selectedPrestation) {
		proxyPrestationDao.delete(selectedPrestation);
		
	}

	@Override
	public void update(PrestationEntity selectedPrestation) {
		proxyPrestationDao.update(selectedPrestation);
		
	}

}
