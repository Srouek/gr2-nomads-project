package fr.afcepf.ai107.appnomads.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.IBusiness.FeedbackIBusiness;
import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.idao.FeedbackMasseurIDao;
import fr.afcepf.ai107.appnomads.idao.FeedbackPartenaireIDao;

@Remote (FeedbackIBusiness.class)
@Stateless
public class FeedbackBusiness implements FeedbackIBusiness
{
	@EJB
	private FeedbackPartenaireIDao proxyFeedbackPartenaireDao;
	
	@EJB
	private FeedbackMasseurIDao proxyFeedbackMasseurDao;
	

	@Override
	public FeedbackPartenaireEntity addFeedbackPartenaire(FeedbackPartenaireEntity feedbackPartenaire) {
		// TODO Auto-generated method stub
		return proxyFeedbackPartenaireDao.add(feedbackPartenaire);
	}

	@Override
	public FeedbackMasseurEntity addFeedbackMasseur(FeedbackMasseurEntity feedbackMasseur) {
		// TODO Auto-generated method stub
		return proxyFeedbackMasseurDao.add(feedbackMasseur);
	}

}
