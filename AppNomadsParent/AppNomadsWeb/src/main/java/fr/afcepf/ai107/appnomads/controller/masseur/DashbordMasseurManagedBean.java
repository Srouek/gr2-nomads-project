package fr.afcepf.ai107.appnomads.controller.masseur;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean (name = "mbDashbordMasseur")
@RequestScoped
public class DashbordMasseurManagedBean {
	
	@EJB
	private MasseurIBusiness proxyMasseurBu;
	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;
	
	@ManagedProperty (value = "mbAccount.template")
	private String template;
	
	private List<PrestationEntity> listePrestationsEffectuees = new ArrayList<PrestationEntity>();
	private List<PrestationEntity> listePrestationsAccessibles = new ArrayList<PrestationEntity>();
	private PrestationEntity selectedPrestation;
	
	@PostConstruct
	public void init() {
		listePrestationsEffectuees = proxyMasseurBu.getListePrestationsByMasseur((MasseurEntity) utilisateur);
		//listePrestationsAccessibles = proxyMasseurBu.getListePrestationsAccessiblesBy
	}
	
	public String showPrestation(int id) {
		
		selectedPrestation = proxyPrestationBusiness.findPrestation(id);
		
		String retour = null;
		if(selectedPrestation != null) {
			retour = "/prestation/prestationPage.xhtml?faces-redirect=true";
			
		}else {
			retour = "/masseur/DashbordMasseur.xhtml?faces-redirect=true";
		}
		
		return retour;
	}

	public DashbordMasseurManagedBean() {
		// TODO Auto-generated constructor stub
	}

	public MasseurIBusiness getProxyMasseurBu() {
		return proxyMasseurBu;
	}

	public void setProxyMasseurBu(MasseurIBusiness proxyMasseurBu) {
		this.proxyMasseurBu = proxyMasseurBu;
	}

	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public List<PrestationEntity> getListePrestationsEffectuees() {
		return listePrestationsEffectuees;
	}

	public void setListePrestationsEffectuees(List<PrestationEntity> listePrestationsEffectuees) {
		this.listePrestationsEffectuees = listePrestationsEffectuees;
	}

	public List<PrestationEntity> getListePrestationsAccessibles() {
		return listePrestationsAccessibles;
	}

	public void setListePrestationsAccessibles(List<PrestationEntity> listePrestationsAccessibles) {
		this.listePrestationsAccessibles = listePrestationsAccessibles;
	}

}
