package fr.afcepf.ai107.appnomads.controller.masseur;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.IBusiness.FeedbackIBusiness;
import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean(name = "mbFeedbackMasseur")
@RequestScoped
public class FeedbackMasseurManagedBean implements Serializable
{

	
	private static final long serialVersionUID = 1L;
	private MasseurEntity masseur = new MasseurEntity();
	private FeedbackMasseurEntity feedbackMasseur = new FeedbackMasseurEntity();
	
	
	

	private List<PrestationEntity> listePrestations;

	@ManagedProperty (value = "#{mbAccount.profilMasseur}")
	private UtilisateurEntity user;
	@ManagedProperty (value = "#{mbMasseurs.selectedPrestation}")
	private PrestationEntity selectedPrestation;
	
	@EJB
	private FeedbackIBusiness proxyFeedbackMasseurBu;
	@EJB
	private PrestationIBusiness proxyPrestationBu;
	@EJB
	private MasseurIBusiness proxyMasseurBu;

	public void AjouterFeedbackMasseur() {
		// Persister le feedback
		
		masseur = proxyMasseurBu.getMasseurById(user.getId());
		feedbackMasseur.setMasseurEntity(masseur);
		feedbackMasseur.setPrestationEntity(selectedPrestation);
		feedbackMasseur.setDateFeedback(new Date());
		feedbackMasseur = proxyFeedbackMasseurBu.addFeedbackMasseur(feedbackMasseur);
		
	}
	
	public String feedbackPage(int id) {
		String retour = null;
		selectedPrestation = proxyPrestationBu.findPrestation(id);
		if(selectedPrestation != null) {
			retour = "/masseur/FeedbackMasseur.xhtml?faces-redirect=true";
		}
		
		return retour;
	}

	@PostConstruct
	public void init() {

//		listePrestations = proxyFeedbackMasseurBu.displayAllPrestation();
	}

	

	public MasseurEntity getMasseur() {
		return masseur;
	}

	public void setMasseur(MasseurEntity masseur) {
		this.masseur = masseur;
	}

	public FeedbackMasseurEntity getFeedbackMasseur() {
		return feedbackMasseur;
	}

	public void setFeedbackMasseur(FeedbackMasseurEntity feedbackMasseur) {
		this.feedbackMasseur = feedbackMasseur;
	}

	public List<PrestationEntity> getListePrestations() {
		return listePrestations;
	}

	public void setListePrestations(List<PrestationEntity> listePrestations) {
		this.listePrestations = listePrestations;
	}
	public PrestationEntity getSelectedPrestation() {
		return selectedPrestation;
	}

	public void setSelectedPrestation(PrestationEntity selectedPrestation) {
		this.selectedPrestation = selectedPrestation;
	}

	public FeedbackIBusiness getProxyFeedbackMasseurBu() {
		return proxyFeedbackMasseurBu;
	}

	public void setProxyFeedbackMasseurBu(FeedbackIBusiness proxyFeedbackMasseurBu) {
		this.proxyFeedbackMasseurBu = proxyFeedbackMasseurBu;
	}

	public PrestationIBusiness getProxyPrestationBu() {
		return proxyPrestationBu;
	}

	public void setProxyPrestationBu(PrestationIBusiness proxyPrestationBu) {
		this.proxyPrestationBu = proxyPrestationBu;
	}

	public UtilisateurEntity getUser() {
		return user;
	}

	public void setUser(UtilisateurEntity user) {
		this.user = user;
	}

	
	public MasseurIBusiness getProxyMasseurBu() {
		return proxyMasseurBu;
	}

	public void setProxyMasseurBu(MasseurIBusiness proxyMasseurBu) {
		this.proxyMasseurBu = proxyMasseurBu;
	}

}