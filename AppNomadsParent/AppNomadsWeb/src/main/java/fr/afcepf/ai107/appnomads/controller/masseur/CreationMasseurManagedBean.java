package fr.afcepf.ai107.appnomads.controller.masseur;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.EvolutionCarriereEntity;
import fr.afcepf.ai107.appnomads.entity.GenreEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.NiveauEntity;
import fr.afcepf.ai107.appnomads.entity.StatutEntity;

@ManagedBean (name = "mbCreationMasseur")
@RequestScoped
public class CreationMasseurManagedBean {
	
	private MasseurEntity masseur = new MasseurEntity();
	private List<GenreEntity> listeGenre;
	private AdresseEntity adresseEntity = new AdresseEntity();
	private HistoriqueActiviteMasseurEntity historiqueActivite = new HistoriqueActiviteMasseurEntity();
	private EvolutionCarriereEntity evolutionCarriere = new EvolutionCarriereEntity();
	private List<EvolutionCarriereEntity> listeEvolution = new ArrayList<EvolutionCarriereEntity>();
	private List<HistoriqueActiviteMasseurEntity> listeHistorique = new ArrayList<HistoriqueActiviteMasseurEntity>();
	private StatutEntity statutEntity = new StatutEntity(2, listeEvolution);
	private NiveauEntity niveauEntity = new NiveauEntity(1, listeEvolution);
	private String dateDeNaissance;
	
	@EJB
	private MasseurIBusiness proxyMasseurBu;
	
	public String AjouterMasseur() {
		//Persister l'adresse
		adresseEntity = proxyMasseurBu.addAdresse(adresseEntity);
		
		listeEvolution.add(evolutionCarriere);
		
		
		
		//Persister l'évolution historique
		evolutionCarriere.setDateDebutEvolution(new Date());
		evolutionCarriere.setNiveauEntity(niveauEntity);
		evolutionCarriere.setStatutEntity(statutEntity);
		evolutionCarriere = proxyMasseurBu.addEvolutionCarriere(evolutionCarriere);
		
		//Persister le masseur
		masseur.setDateEmbauche(new Date());
		try {
			masseur.setDateDeNaissance(new SimpleDateFormat("yyyy-MM-dd").parse(dateDeNaissance));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		masseur.setEvolutionCarriereEntity(listeEvolution);
		masseur.setHistoriqueActiviteMasseurEntity(listeHistorique);
		masseur.setAdresseEntity(adresseEntity);
		masseur.setLogin(masseur.getPrenom());
		masseur.setPassword(masseur.getPrenom());
		masseur = proxyMasseurBu.addMasseur(masseur);
		
		//Persister l'historique d'activité
		historiqueActivite.setDateDebutActivite(new Date());
		historiqueActivite.setMasseur(masseur);
		historiqueActivite = proxyMasseurBu.addHistoriqueActivite(historiqueActivite);
		return "/masseur/ListeMasseurs.xhtml?faces-redirect=true";
	}
	@PostConstruct
	public void init() {
		
		listeGenre = proxyMasseurBu.displayAllGenre();
	}
	public CreationMasseurManagedBean() {
		
	}

	public MasseurEntity getMasseur() {
		return masseur;
	}

	public void setMasseur(MasseurEntity masseur) {
		this.masseur=masseur;
	}
	public List<GenreEntity> getListeGenre() {
		return listeGenre;
	}
	public void setListeGenre(List<GenreEntity> listeGenre) {
		this.listeGenre = listeGenre;
	}
	public MasseurIBusiness getProxyCreationMasseurBu() {
		return proxyMasseurBu;
	}
	public void setProxyCreationMasseurBu(MasseurIBusiness proxyMasseurBu) {
		this.proxyMasseurBu = proxyMasseurBu;
	}
	public AdresseEntity getAdresseEntity() {
		return adresseEntity;
	}
	public void setAdresseEntity(AdresseEntity adresseEntity) {
		this.adresseEntity = adresseEntity;
	}
	public String getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(String dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	

}
