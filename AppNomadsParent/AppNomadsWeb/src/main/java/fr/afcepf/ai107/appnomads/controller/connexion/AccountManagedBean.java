package fr.afcepf.ai107.appnomads.controller.connexion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.IBusiness.PartenaireIBusiness;
import fr.afcepf.ai107.appnomads.IBusiness.connexion.UtilisateurIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdministrateurEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PreferencesEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;

@ManagedBean(name = "mbAccount")
@SessionScoped
public class AccountManagedBean implements Serializable
{

	private static final long serialVersionUID = 1L;
	private HttpSession session;

	private UtilisateurEntity utilisateur;
	private String login;
	private String password;
	private String template;
	private String role;
	private String cssSheet;
	private String activite;
	private String niveau;
	private String statut;
	private List<HistoriqueActivitePartenaireEntity> listeHistoriqueActivitePartenaire;
	private List<HistoriqueActiviteMasseurEntity> listeHistoriqueActiviteMasseur;
	private List<PreferencesEntity> listePreferences;
	
	private AdministrateurEntity profilAdmin;
	private PartenaireEntity profilPartenaire;
	private MasseurEntity profilMasseur;

	@EJB
	private UtilisateurIBusiness proxyUtilisateurBusiness;
	
	@EJB
	private MasseurIBusiness proxyMasseurBu;
	
	@EJB
	private PartenaireIBusiness proxyPartenairesBu;
	

	public String connexion() {

		utilisateur = proxyUtilisateurBusiness.connexion(login, password);
		String retour = null;
		
		if (utilisateur != null)
		{
			if (isAdmin() != false)
			{
				template = "../templates/TemplateAdmin.xhtml";
				setRole("Administrateur");
				profilAdmin = proxyUtilisateurBusiness.getAdminById(utilisateur.getId());
				cssSheet = "/CSS/adminstyle.css";
				retour = "/admin/DashbordAdmin.xhtml?faces-redirect=true";
				
			} else if (isMasseur())
			{
				ChargerDonnéesMasseur();
				retour = "/masseur/DashbordMasseur.xhtml?faces-redirect=true";
			} else if (isPartenaire())
			{
				ChargerDonnéesPartenaire();
				retour = "/partenaire/DashbordPartenaire.xhtml?faces-redirect=true";
			} else
			{
				
				retour = "/Login.xhtml?faces-redirect=true";

			}
		}
		return retour;
	}
	public void ChargerDonnéesMasseur() {
		template = "../templates/TemplateMasseur.xhtml";
		setRole("Masseur");
		profilMasseur = proxyUtilisateurBusiness.getMasseurById(utilisateur.getId());
		listeHistoriqueActiviteMasseur = proxyMasseurBu.getListeHistoriqueActiviteByMasseur(profilMasseur);
		cssSheet = "/CSS/masseurstyle.css";
		activite = isActif(profilMasseur);
		niveau = getNiveauMasseur(profilMasseur);
		statut = getStatutMasseur(profilMasseur);
		listePreferences = proxyMasseurBu.getListePreferencesByMasseur(profilMasseur);
	}
	public void ChargerDonnéesPartenaire() {
		template = "../templates/TemplatePartenaire.xhtml";
		setRole("Partenaire");
		profilPartenaire = proxyUtilisateurBusiness.getPartenaireById(utilisateur.getId());
		listeHistoriqueActivitePartenaire = proxyPartenairesBu.getListeHistoriqueActiviteByPartenaire(profilPartenaire);
		cssSheet = "/CSS/partenairestyle.css";
	}
	public Boolean isAdmin() {
		if (utilisateur == null)
		{
			utilisateur = new UtilisateurEntity();
		}
		
		return utilisateur.getClass() == AdministrateurEntity.class;
	}

	public Boolean isMasseur() {
		if (utilisateur == null)
		{
			utilisateur = new UtilisateurEntity();
			
		}
		return utilisateur.getClass() == MasseurEntity.class;
	}

	public Boolean isPartenaire() {
		if (utilisateur == null)
		{
			utilisateur = new UtilisateurEntity();
			
		}
		return utilisateur.getClass() == PartenaireEntity.class;
	}

	
    public String deconnexion() {


    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    	session.invalidate();
    	login ="";
    	password="";
    	utilisateur = new UtilisateurEntity();

    	return "../Login.xhtml?faces-redirect=true";
    }
    
    public String isActif(MasseurEntity selectedMasseur) {
		boolean isActif = proxyMasseurBu.isActif(selectedMasseur);
		if (isActif) {
			return "Actif";
		}
		else {
			return "Inactif";
		}
	}
	public String getNiveauMasseur(MasseurEntity selectedMasseur) {
		return proxyMasseurBu.getNiveauByMasseur(selectedMasseur).getLibelle();
	}
	
	public String getStatutMasseur(MasseurEntity selectedMasseur) {
		return proxyMasseurBu.getStatutByMasseur(selectedMasseur).getLibelle();
	}
	
	public String updateMasseur() {
		proxyMasseurBu.updateMasseur(profilMasseur);
		return "/masseur/DashbordMasseur.xhtml?faces-redirect=true";
	}
	public String updatePartenaire() {
		proxyPartenairesBu.updatePartenaire(profilPartenaire);
		return "/partenaire/DashbordPartenaire.xhtml?faces-redirect=true";
	}
	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public AdministrateurEntity getAdmin() {
		return profilAdmin;
	}

	public void setAdmin(AdministrateurEntity admin) {
		this.profilAdmin = admin;
	}

	public PartenaireEntity getPartenaire() {
		return profilPartenaire;
	}

	public void setPartenaire(PartenaireEntity partenaire) {
		this.profilPartenaire = partenaire;
	}

	public MasseurEntity getMasseur() {
		return profilMasseur;
	}

	public void setMasseur(MasseurEntity masseur) {
		this.profilMasseur = masseur;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCssSheet() {
		return cssSheet;
	}

	public void setCssSheet(String cssSheet) {
		this.cssSheet = cssSheet;
	}

	public AdministrateurEntity getProfilAdmin() {
		return profilAdmin;
	}

	public void setProfilAdmin(AdministrateurEntity profilAdmin) {
		this.profilAdmin = profilAdmin;
	}

	public PartenaireEntity getProfilPartenaire() {
		return profilPartenaire;
	}

	public void setProfilPartenaire(PartenaireEntity profilPartenaire) {
		this.profilPartenaire = profilPartenaire;
	}

	public MasseurEntity getProfilMasseur() {
		return profilMasseur;
	}

	public void setProfilMasseur(MasseurEntity profilMasseur) {
		this.profilMasseur = profilMasseur;
	}

	public UtilisateurIBusiness getProxyUtilisateurBusiness() {
		return proxyUtilisateurBusiness;
	}

	public void setProxyUtilisateurBusiness(UtilisateurIBusiness proxyUtilisateurBusiness) {
		this.proxyUtilisateurBusiness = proxyUtilisateurBusiness;
	}

	public String getActivite() {
		return activite;
	}

	public void setActivite(String activite) {
		this.activite = activite;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public List<HistoriqueActivitePartenaireEntity> getListeHistoriqueActivitePartenaire() {
		return listeHistoriqueActivitePartenaire;
	}

	public void setListeHistoriqueActivitePartenaire(
			List<HistoriqueActivitePartenaireEntity> listeHistoriqueActivitePartenaire) {
		this.listeHistoriqueActivitePartenaire = listeHistoriqueActivitePartenaire;
	}

	public List<HistoriqueActiviteMasseurEntity> getListeHistoriqueActiviteMasseur() {
		return listeHistoriqueActiviteMasseur;
	}

	public void setListeHistoriqueActiviteMasseur(List<HistoriqueActiviteMasseurEntity> listeHistoriqueActiviteMasseur) {
		this.listeHistoriqueActiviteMasseur = listeHistoriqueActiviteMasseur;
	}

	public MasseurIBusiness getProxyMasseurBu() {
		return proxyMasseurBu;
	}

	public void setProxyMasseurBu(MasseurIBusiness proxyMasseurBu) {
		this.proxyMasseurBu = proxyMasseurBu;
	}

	public PartenaireIBusiness getProxyPartenairesBu() {
		return proxyPartenairesBu;
	}

	public void setProxyPartenairesBu(PartenaireIBusiness proxyPartenairesBu) {
		this.proxyPartenairesBu = proxyPartenairesBu;
	}

	public List<PreferencesEntity> getListePreferences() {
		return listePreferences;
	}

	public void setListePreferences(List<PreferencesEntity> listePreferences) {
		this.listePreferences = listePreferences;
	}
	

}
