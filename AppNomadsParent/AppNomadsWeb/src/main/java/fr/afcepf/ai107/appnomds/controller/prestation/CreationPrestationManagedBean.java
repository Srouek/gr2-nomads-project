package fr.afcepf.ai107.appnomds.controller.prestation;

import java.io.Serializable;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.ReccurenceEntity;
import fr.afcepf.ai107.appnomads.entity.TypePaiementEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean (name = "mbCreationPrestation")
@RequestScoped
public class CreationPrestationManagedBean implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	private PrestationEntity fetchPrestation = new PrestationEntity();
	private PrestationEntity laPrestation;
	private AdresseEntity adresse = new AdresseEntity();
	private ReccurenceEntity reccurence = new ReccurenceEntity();
	private List<PartenaireEntity> listPartenaires;
	private List<TypePaiementEntity> typePaiment;
	private PrestationEntity updatePrestation;
	private String datePrestation;
	private String heureDebut;
	private String heureFin;
	private String nbMasseur;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	
	@EJB
	private PrestationIBusiness proxyPrestationBu;
	
	@PostConstruct
	public void onLoad() {
		listPartenaires = proxyPrestationBu.displayPartenaires();
		typePaiment = proxyPrestationBu.displayTypePaiement();
		
	}
	
	
	public String createNewPrestation() {
		String retour = null;
		if(fetchPrestation == null) {
		
			return retour = "/prestation/newPrestation.xhtml?faces-redirect=true";
			
		}else {
			
			try
			{
				SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
				
				
				fetchPrestation.setDatePrestation( formatterDate.parse(datePrestation));
				fetchPrestation.setHeureDebut(StringToHour(heureDebut));
				fetchPrestation.setHeureFin(StringToHour(heureFin));
				
			} catch (ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			adresse = proxyPrestationBu.ajouterAdresse(adresse);
			reccurence = proxyPrestationBu.ajouterReccurence(reccurence);
			fetchPrestation.setAdresseEntity(adresse);
			fetchPrestation.setReccurence(reccurence);
			fetchPrestation.setIdPrestationMere(fetchPrestation.getId());
			fetchPrestation.setNbMasseur(Integer.parseInt(nbMasseur));
			
			laPrestation = proxyPrestationBu.createPrestation(fetchPrestation);
			
			retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
			
		}
		return retour;
		
	}
	
	public void finReccurence(PrestationEntity prestation) {
		if(prestation != null) {
			
			Calendar cal = Calendar.getInstance();
			Calendar ecart = Calendar.getInstance();
			cal.setTime(prestation.getDatePrestationDate());
			cal.add(Calendar.MONTH, prestation.getReccurence().getDureeReccurence());
			
			if(prestation.getDatePrestationDate().before(cal.getTime())) {
				
				ecart.add(Calendar.DATE, prestation.getReccurence().getEcartReccurence());
				prestation.setIdPrestationMere(prestation.getId());
				prestation.setId(null);
				prestation.setDatePrestation(cal.getTime());
				proxyPrestationBu.createPrestation(prestation);
				
			}
		}
		
	}
	/*********************************** Static méthode for convertion ***********************************/
	
	@SuppressWarnings("deprecation")
	static Time StringToHour(String string) {
		String[] parts = string.split(":");
		int heure = Integer.parseInt(parts[0]); 
		int minute = Integer.parseInt(parts[1]);
		int second = 00;
		Time tmp = new Time(heure, minute, second);
		
		return tmp;
	}
	
	
	
	/*****************************************************************************************************/

	
	
	/********* Getter Setter *********/
	public PrestationEntity getFetchPrestation() {
		return fetchPrestation;
	}

	public void setFetchPrestation(PrestationEntity fetchPrestation) {
		this.fetchPrestation = fetchPrestation;
	}

	public PrestationEntity getUpdatePrestation() {
		return updatePrestation;
	}

	public void setUpdatePrestation(PrestationEntity updatePrestation) {
		this.updatePrestation = updatePrestation;
	}

	




	public String getDatePrestation() {
		return datePrestation;
	}




	public void setDatePrestation(String datePrestation) {
		this.datePrestation = datePrestation;
	}




	public String getHeureDebut() {
		return heureDebut;
	}




	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}




	public String getHeureFin() {
		return heureFin;
	}




	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}




	public String getNbMasseur() {
		return nbMasseur;
	}




	public void setNbMasseur(String nbMasseur) {
		this.nbMasseur = nbMasseur;
	}


	public AdresseEntity getAdresse() {
		return adresse;
	}


	public void setAdresse(AdresseEntity adresse) {
		this.adresse = adresse;
	}


	public List<PartenaireEntity> getListPartenaires() {
		return listPartenaires;
	}


	public void setListPartenaires(List<PartenaireEntity> listPartenaires) {
		this.listPartenaires = listPartenaires;
	}


	public ReccurenceEntity getReccurence() {
		return reccurence;
	}


	public void setReccurence(ReccurenceEntity reccurence) {
		this.reccurence = reccurence;
	}


	public List<TypePaiementEntity> getTypePaiment() {
		return typePaiment;
	}


	public void setTypePaiment(List<TypePaiementEntity> typePaiment) {
		this.typePaiment = typePaiment;
	}


	public PrestationEntity getLaPrestation() {
		return laPrestation;
	}


	public void setLaPrestation(PrestationEntity laPrestation) {
		this.laPrestation = laPrestation;
	}


	public SimpleDateFormat getSdf() {
		return sdf;
	}


	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}
	
	

}
