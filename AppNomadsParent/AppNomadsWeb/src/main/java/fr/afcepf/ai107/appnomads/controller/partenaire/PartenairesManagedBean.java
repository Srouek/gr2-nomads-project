package fr.afcepf.ai107.appnomads.controller.partenaire;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import fr.afcepf.ai107.appnomads.IBusiness.PartenaireIBusiness;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean (name = "mbPartenaires")
@SessionScoped
public class PartenairesManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private PartenaireEntity selectedPartenaire;
	private PrestationEntity selectedPrestation;
	private List<PartenaireEntity> listePartenaires;
	private List<FeedbackPartenaireEntity> feedbackPartenaires;
	private List<HistoriqueActivitePartenaireEntity> listeHistoriqueActivite;
	private List<PrestationEntity> prestationDuPartenaire;

	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;
	
	@EJB
	private PartenaireIBusiness proxyPartenairesBu;
	@EJB
	private PrestationIBusiness proxyPrestationBu;
	
	@PostConstruct
	public void init() {
		listePartenaires = proxyPartenairesBu.findAll();
		prestationDuPartenaire = proxyPartenairesBu.getListePrestationsByPartenaire(connectedPartenaire(utilisateur.getId()));
		
	}
	
	public String feedbackPage(int id) {
		String retour = null;
		selectedPrestation = proxyPrestationBu.findPrestation(id);
		if(selectedPrestation != null) {
			retour = "/partenaire/FeedbackPartenaire.xhtml?faces-redirect=true";
		}
		
		return retour;
	}
	
	public PartenaireEntity connectedPartenaire(Integer id) {
		PartenaireEntity result = proxyPartenairesBu.getPartenaireById(id);
		
		return result;
	}
	
	public String updatePartenaire() {
		proxyPartenairesBu.updatePartenaire(selectedPartenaire);
		return "/partenaire/DashbordPartenaire.xhtml?faces-redirect=true";
	}
	public String desactiverPartenaire() {
		proxyPartenairesBu.desactivatePartenaire(selectedPartenaire);
		return "/partenaire/DashbordPartenaire.xhtml?faces-redirect=true";
	}
	public PartenaireIBusiness getproxyPartenairesBu() {
		return proxyPartenairesBu;
	}
	public void setproxyPartenairesBu(PartenaireIBusiness proxyPartenairesBu) {
		this.proxyPartenairesBu = proxyPartenairesBu;
	}
	
	public PartenaireEntity getSelectedPartenaire() {
		return selectedPartenaire;
	}
	public void setSelectedPartenaire(PartenaireEntity selectedPartenaire) {
		this.selectedPartenaire = selectedPartenaire;
	}
	public List<PartenaireEntity> getListePartenaires() {
		return listePartenaires;
	}
	public void setListePartenaires(List<PartenaireEntity> listePartenaires) {
		this.listePartenaires = listePartenaires;
	}
	
	public String showPartenaire(Integer id) {

		selectedPartenaire = proxyPartenairesBu.getPartenaireById(id);
		String retour = null;
		if(selectedPartenaire != null) {
			System.out.println("Dans diff null "+ selectedPartenaire.getNomCourant());
			listeHistoriqueActivite = proxyPartenairesBu.getListeHistoriqueActiviteByPartenaire(selectedPartenaire);
			retour = "/partenaire/ProfilPartenaire.xhtml?faces-redirect=true";
		}else {
			retour = "/partenaire/ListePartenaires.xhtml?faces-redirect=true";
		}
		
		return retour;
	}

	public List<FeedbackPartenaireEntity> getFeedbackPartenaires() {
		return feedbackPartenaires;
	}

	public void setFeedbackPartenairesOfPresta(List<FeedbackPartenaireEntity> feedbackPartenaires) {
		this.feedbackPartenaires = feedbackPartenaires;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setFeedbackPartenaires(List<FeedbackPartenaireEntity> feedbackPartenaires) {
		this.feedbackPartenaires = feedbackPartenaires;
	}

	public List<HistoriqueActivitePartenaireEntity> getListeHistoriqueActivite() {
		return listeHistoriqueActivite;
	}

	public void setListeHistoriqueActivite(List<HistoriqueActivitePartenaireEntity> listeHistoriqueActivite) {
		this.listeHistoriqueActivite = listeHistoriqueActivite;
	}

	public PartenaireIBusiness getProxyPartenairesBu() {
		return proxyPartenairesBu;
	}

	public void setProxyPartenairesBu(PartenaireIBusiness proxyPartenairesBu) {
		this.proxyPartenairesBu = proxyPartenairesBu;
	}

	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<PrestationEntity> getPrestationDuPartenaire() {
		return prestationDuPartenaire;
	}

	public void setPrestationDuPartenaire(List<PrestationEntity> prestationDuPartenaire) {
		this.prestationDuPartenaire = prestationDuPartenaire;
	}

	public PrestationEntity getSelectedPrestation() {
		return selectedPrestation;
	}

	public void setSelectedPrestation(PrestationEntity selectedPrestation) {
		this.selectedPrestation = selectedPrestation;
	}

	
	
	
	
	
}
