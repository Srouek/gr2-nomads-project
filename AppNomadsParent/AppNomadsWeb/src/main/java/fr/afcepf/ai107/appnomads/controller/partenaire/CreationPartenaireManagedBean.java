package fr.afcepf.ai107.appnomads.controller.partenaire;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.IBusiness.PartenaireIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.TypePartenaireEntity;

@ManagedBean (name="mbCreationPartenaire")
@RequestScoped
public class CreationPartenaireManagedBean {
	
	private PartenaireEntity partenaire = new PartenaireEntity();
	private List<TypePartenaireEntity> listeTypePartenaire;
	private AdresseEntity adresseEntity = new AdresseEntity();
	private HistoriqueActivitePartenaireEntity historiqueActivite = new HistoriqueActivitePartenaireEntity();
	private List<HistoriqueActivitePartenaireEntity> listeHistorique = new ArrayList<HistoriqueActivitePartenaireEntity>();

	private String dateCreationPartenariat;
	private String personneContact;
	
	@EJB
	private PartenaireIBusiness proxyPartenaireBu;
	
	public String AjouterPartenaire() {
		
		//Persister le partenaire
		adresseEntity = proxyPartenaireBu.addAdresse(adresseEntity);

		
		partenaire.setDateCreationPartenaire(new Date());
		
		partenaire.setPersonneContact(personneContact);
		partenaire.setHistoriqueActivitePartenaireEntity(listeHistorique);
		partenaire.setAdresseEntity(adresseEntity);

		partenaire.setLogin(partenaire.getNomCourant());
		partenaire.setPassword(partenaire.getPersonneContact());
		
		partenaire = proxyPartenaireBu.addPartenaire(partenaire);
		//ajouter connextion avec toutes les tables - historique avec datedebut activite + activite
	
		//Persister l'historique d'activité
		historiqueActivite.setDateDebutActivite(new Date());
		historiqueActivite.setPartenaire(partenaire);
		historiqueActivite = proxyPartenaireBu.addHistoriqueActivitePartenaire(historiqueActivite);
		
		return "/partenaire/ListePartenaires.xhtml?faces-redirect=true";

	
	}
	
		@PostConstruct
		public void init() {
			
			listeTypePartenaire = proxyPartenaireBu.displayAllTypePartenaire();
		}
		
		public PartenaireEntity getPartenaire() {
			return partenaire;
		}
		public void setPartenaire(PartenaireEntity partenaire) {
			this.partenaire = partenaire;
		}
		public List<TypePartenaireEntity> getListeTypePartenaire() {
			return listeTypePartenaire;
		}
		public void setListeTypePartenaire(List<TypePartenaireEntity> listeTypePartenaire) {
			this.listeTypePartenaire = listeTypePartenaire;
		}
		public String getDateCreationPartenariat() {
			return dateCreationPartenariat;
		}
		public void setDateCreationPartenariat(String dateCreationPartenariat) {
			this.dateCreationPartenariat = dateCreationPartenariat;
		}
		public String getPersonneContact() {
			return personneContact;
		}
		public void setPersonneContact(String personneContact) {
			this.personneContact = personneContact;
		}
		public PartenaireIBusiness getProxyPartenaireBu() {
			return proxyPartenaireBu;
		}
		public void setProxyPartenaireBu(PartenaireIBusiness proxyPartenaireBu) {
			this.proxyPartenaireBu = proxyPartenaireBu;
		}
		public CreationPartenaireManagedBean() {
	
		}

		public AdresseEntity getAdresseEntity() {
			return adresseEntity;
		}

		public void setAdresseEntity(AdresseEntity adresseEntity) {
			this.adresseEntity = adresseEntity;
		}

		public HistoriqueActivitePartenaireEntity getHistoriqueActivite() {
			return historiqueActivite;
		}

		public void setHistoriqueActivite(HistoriqueActivitePartenaireEntity historiqueActivite) {
			this.historiqueActivite = historiqueActivite;
		}

		public List<HistoriqueActivitePartenaireEntity> getListeHistorique() {
			return listeHistorique;
		}

		public void setListeHistorique(List<HistoriqueActivitePartenaireEntity> listeHistorique) {
			this.listeHistorique = listeHistorique;
		}
		


	

}
