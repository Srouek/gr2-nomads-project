package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.ReccurenceEntity;

public interface ReccurenceIDao extends GenericIDao<ReccurenceEntity>
{

}
