package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.TypePartenaireEntity;

public interface TypePartenaireIDao extends GenericIDao<TypePartenaireEntity> {

}
