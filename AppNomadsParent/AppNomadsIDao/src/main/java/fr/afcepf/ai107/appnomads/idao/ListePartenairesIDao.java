package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;

public interface ListePartenairesIDao extends GenericIDao<PartenaireEntity>{
	
	PartenaireEntity init();
	
}
