package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;

public interface ListeMasseursIDao extends GenericIDao<MasseurEntity>{
	
	MasseurEntity init();
	
}
