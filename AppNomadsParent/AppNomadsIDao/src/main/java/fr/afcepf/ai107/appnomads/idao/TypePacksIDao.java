package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.TypePacksEntity;

public interface TypePacksIDao extends GenericIDao<TypePacksEntity> {

}
