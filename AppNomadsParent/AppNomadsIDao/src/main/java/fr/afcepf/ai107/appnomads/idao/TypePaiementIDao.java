package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.TypePaiementEntity;

public interface TypePaiementIDao extends GenericIDao<TypePaiementEntity>
{

}
