package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PreferencesEntity;

public interface PreferencesIDao extends GenericIDao<PreferencesEntity> {

	List<PreferencesEntity> getListePreferencesByMasseur(MasseurEntity masseur);

}
