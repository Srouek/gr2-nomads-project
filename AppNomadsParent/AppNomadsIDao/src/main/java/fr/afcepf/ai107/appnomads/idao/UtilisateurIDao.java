package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;

public interface UtilisateurIDao extends GenericIDao<UtilisateurEntity>{

	UtilisateurEntity authenticate(String login, String password);

}
