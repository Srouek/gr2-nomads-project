package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;

public interface AdresseIDao extends GenericIDao<AdresseEntity>{

	int getLastId();
}
