package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.LibelleMaterielEntity;

public interface LibelleMaterielIDao extends GenericIDao<LibelleMaterielEntity> {

}
