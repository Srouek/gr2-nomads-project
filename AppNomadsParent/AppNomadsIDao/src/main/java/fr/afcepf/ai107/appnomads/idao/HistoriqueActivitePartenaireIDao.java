package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;

public interface HistoriqueActivitePartenaireIDao extends GenericIDao<HistoriqueActivitePartenaireEntity>{

	List<HistoriqueActivitePartenaireEntity> getListeHistoriqueActiviteByPartenaire(
			PartenaireEntity selectedPartenaire);

	HistoriqueActivitePartenaireEntity getCurrentActivityByPartenaire(PartenaireEntity selectedPartenaire);

}
