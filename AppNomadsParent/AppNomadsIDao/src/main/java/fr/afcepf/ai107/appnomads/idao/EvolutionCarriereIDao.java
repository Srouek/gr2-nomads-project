package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.EvolutionCarriereEntity;

public interface EvolutionCarriereIDao extends GenericIDao<EvolutionCarriereEntity> {

}
