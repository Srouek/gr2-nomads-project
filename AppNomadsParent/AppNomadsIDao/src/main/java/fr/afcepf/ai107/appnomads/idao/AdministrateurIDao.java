package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.AdministrateurEntity;

public interface AdministrateurIDao extends GenericIDao<AdministrateurEntity>{
}
